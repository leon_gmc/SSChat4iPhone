# SSChat

#### 介绍

语言: Objective-C

客户端: iOS

通讯框架: XMPP

服务端: Openfire

服务端数据库: mysql

移动端数据库: coreData

项目管理: cocoaPod

三方库依赖: Masonry

#### 软件架构
常规MVC

#### 安装教程
### 服务端配置
1. 安装mysql, 地址: [https://www.mysql.com]()
2. 安装openfire, 地址: [http://www.openfire.net.cn]()
3. Openfire安装完毕后, 打开系统偏好设置, 底栏出现Openfire图标, 点击图标后进入server栏,点击开启服务(start Openfire)
   Openfire出现无法启动情况按照如下步骤操作:
   
        ①：sudo chmod -R 777 /usr/local/openfire/bin
        ②：sudo su
        ③：cd /usr/local/openfire/bin
        ④：export JAVA_HOME='/usr/libexec/java_home' "将'' 替换为ESC下方的符号``"
        ⑤：echo $JAVA_HOME /Library/Java/JavaVirtualMachines/jdk1.8.0_40.jdk/Contents/Home
        ⑥: cd /usr/local/openfire/bin
        ⑦: ./openfire.sh
        
4. mysql初始化, connection Method-> standard(TCP/IP) Hostname: (本机地址->127.0.0.1/服务端地址->?.?.?.) username->自定义数据库名

        数据库名字与后面Openfire连接的数据库名保持一致, 登陆账户与密码也保持一致

5. 系统偏好设置, 点击Openfire图标, 点击Administration栏, 点击Open Admin Console, 进入Openfire服务端配置页面
6. 服务端配置页面:
        * 语言选择: 中文
        * 服务器配置: 域: 连接通讯域(自定义) example: com.leon
        * 数据库设置: 标准数据库 数据库URL: 'host-name'->127.0.0.1, 'database-name'->自定义数据库名
        * 用户名, 密码: 数据库用户名, 密码
        * 外形设置: 初始设置
        * 管理员账户: 账户, 密码
        * 登陆管理控制台: 输入账户, 密码
7. 服务器账户配置
        * 进入控制台页面, 点击用户/组栏, 进入栏
        * 选择用户栏, 点击新建用户, 填写用户名, 密码
        * 创建成功, 点击用户栏, 刷新当前页         
### 客户端配置:
1. 测试Openfire客户端: Adium 地址: [https://www.adium.im]() 
        * 打开Adium
        * 左上角Admin中的偏好设置
        * 点击账号, 左下方'+'按钮
        * 点击下拉箭头, 查找XMPP
        * 选中XMPP, 输入JabberID(账号), 密码
        * 点击菜单栏选项按钮, 设置连接服务器: 127.0.0.1(本机) / ?.?.?.?(服务端)
        * 弹出授权页面, 点击授权登陆成功

#### 使用说明

1. clone或download项目
2. 双击SSChat.xcworkspace
3. 查找Tools/Fragmentary.h, 修改useHostName -> 127.0.0.1(本地) / ?.?.?.?(服务端)
4. 查找Tools/Fragmentary.h, 修改userDomain -> Openfire 配置域名
5. run, 登陆账户, 密码 
    账户不需要填写域名, 例如 jid为test@test.com 账户为:test @test.com在userDomain中配置好即可
6. 如果使用真机且服务器为本地环境下, 会出现无法连接的问题, 将useHostName 改成电脑ip
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request



