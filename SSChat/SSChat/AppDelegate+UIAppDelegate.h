//
//  AppDelegate+UIAppDelegate.h
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (UIAppDelegate)
- (void)initWindow;

@end

NS_ASSUME_NONNULL_END
