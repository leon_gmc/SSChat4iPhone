//
//  AppDelegate+UIAppDelegate.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "AppDelegate+UIAppDelegate.h"
#import "Base/SSTabBarController.h"
#import "SSLoginManager.h"
#import "SSBaseViewController.H"

@implementation AppDelegate (UIAppDelegate)

- (void)initWindow {
    self.window = [[UIWindow alloc]initWithFrame:UIScreen.mainScreen.bounds];
    SSTabBarController *tab = [[SSTabBarController alloc]init];
    self.window.rootViewController = [SSBaseViewController new];
    [self.window makeKeyAndVisible];
    
    if (![[SSLoginManager standard] checLogin]) {
        [[SSLoginManager standard] showLogin:^(BOOL isSuccess) {
            if (isSuccess) {
                self.window.rootViewController = nil;
                self.window.rootViewController = tab;
            }
        }];
    } else {
        self.window.rootViewController = tab;
    }
}


@end
