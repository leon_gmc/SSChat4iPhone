//
//  SSBaseCollectionViewCell.h
//  SSChat
//
//  Created by Gmc on 2019/6/2.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSBaseCollectionViewCell : UICollectionViewCell

+(NSString *)getIdentify;

@end

NS_ASSUME_NONNULL_END
