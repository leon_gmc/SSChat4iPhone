//
//  SSBaseCollectionViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/6/2.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseCollectionViewCell.h"

@implementation SSBaseCollectionViewCell

+ (NSString *)getIdentify {
    return [NSString stringWithFormat:@"%@+identify", self];
}

@end
