//
//  SSBaseModel.h
//  SSChat
//
//  Created by Gmc on 2019/5/24.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSBaseModel : NSObject

- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
