//
//  SSBaseTableViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"

@implementation SSBaseTableViewCell
+ (NSString *)getIdentify {
    return [NSString stringWithFormat:@"%@+identify", self];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


@end
