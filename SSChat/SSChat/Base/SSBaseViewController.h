//
//  SSBaseViewController.h
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSBaseViewController : UIViewController


- (void)sendMessageToParent:(NSObject *)object;

@end

NS_ASSUME_NONNULL_END
