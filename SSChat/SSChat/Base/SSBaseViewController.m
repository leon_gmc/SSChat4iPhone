//
//  SSBaseViewController.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseViewController.h"

@interface SSBaseViewController ()

@end

@implementation SSBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColor.whiteColor;
}


- (void)sendMessageToParent:(NSObject *)object {
    if (self.parentViewController.childViewControllers.count > 1) {
        ///
        NSInteger selfIndex = [self.parentViewController.childViewControllers indexOfObject:self];
        SSBaseViewController *parent = [self.parentViewController.childViewControllers objectAtIndex:selfIndex - 1];
        [parent sendMessageToParent:object];
    }

}

@end
