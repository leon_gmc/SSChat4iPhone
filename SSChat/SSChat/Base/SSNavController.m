//
//  SSNavController.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSNavController.h"
#import "UIImage+SSImage.h"
#import "UIColor+SSColor.h"
#import "Fragmentary.h"

@interface SSNavController ()

@end

@implementation SSNavController

+ (void)initialize {
    UINavigationBar *bar = [UINavigationBar appearance];
//    [bar setBackgroundImage:[self getImage] forBarMetrics:UIBarMetricsDefault];
    [bar setBackgroundImage:[UIImage imageWithColor:UIColor.blackColor size:CGSizeMake(W, H)] forBarMetrics:UIBarMetricsDefault];
    [bar setTitleTextAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:16], NSForegroundColorAttributeName : UIColor.blackColor}];
    [bar setTintColor:[UIColor whiteColor]];
    [bar setTitleTextAttributes:@{NSForegroundColorAttributeName: UIColor.whiteColor}];
    //    [bar setBackgroundColor:[UIColor colorWithHexString:@"#F48FB6"]];
    // 设置item
    UIBarButtonItem *item = [UIBarButtonItem appearance];
    // UIControlStateNormal
    NSMutableDictionary *itemAttrs = [NSMutableDictionary dictionary];
    itemAttrs[NSFontAttributeName] = [UIFont systemFontOfSize:17];
    itemAttrs[NSForegroundColorAttributeName] = [UIColor whiteColor];
    [item setTitleTextAttributes:itemAttrs forState:UIControlStateNormal];
    
    // UIControlStateDisabled
    NSMutableDictionary *itemDisabledAttrs = [NSMutableDictionary dictionary];
    itemDisabledAttrs[NSForegroundColorAttributeName] = [UIColor lightGrayColor];
    [item setTitleTextAttributes:itemDisabledAttrs forState:UIControlStateDisabled];
    
}

+ (UIImage *)getImage {
    UIView *back = [[UIView alloc]initWithFrame:CGRectMake(0, 0, W, H)];
    back.backgroundColor = UIColor.blackColor;
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#FF6E7F"].CGColor, (__bridge id)[UIColor colorWithHexString:@"#BFE9FF"].CGColor];
    gradientLayer.locations = @[@0.3, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1.0, 0);
    gradientLayer.frame = back.frame;
    [back.layer addSublayer:gradientLayer];
    UIGraphicsBeginImageContext(back.bounds.size);
    [back.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if (self.viewControllers.count == 1) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    
    [super pushViewController:viewController animated:animated];
}

- (UIViewController *)popViewControllerAnimated:(BOOL)animated {
    return [super popViewControllerAnimated:animated];
}


@end
