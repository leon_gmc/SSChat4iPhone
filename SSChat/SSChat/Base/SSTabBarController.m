//
//  SSTabBarController.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSTabBarController.h"
#import "UIColor+SSColor.h"
#import "GMCRoute.h"

@interface SSTabBarController ()

@end

@implementation SSTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadSub];
//    UIColor *titleNormalColor = [UIColor colorWithHexString:@"#778899"];
    UIColor *titleNormalColor = [UIColor grayColor];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleNormalColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateNormal];
//    UIColor *titleHighlightedColor = [UIColor colorWithHexString:@"#F9D558"];
    UIColor *titleHighlightedColor = [UIColor whiteColor];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, NSForegroundColorAttributeName,
                                                       nil] forState:UIControlStateSelected];
    
    [UITabBarItem appearance].titlePositionAdjustment = UIOffsetMake(0, 1);
    [[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:UIColor.blackColor size:CGSizeMake(W, TabbarHeight)]];
    
}

+ (void)initialize {
    
    

}

- (void)loadSub {
    [[GMCRoute shared] loadRootList:@"root" root:self];
}

@end
