//
//  SSChatMainController.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSChatMainController.h"
#import "SSChatListCell.h"
#import "GMCRoute.h"
#import "XMPPMessageArchiving_Contact_CoreDataObject.h"
#import "GMCXMPPManager.h"

@interface SSChatMainController ()<UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, GMCXMPPManagerPersonalInfoProtocol>
@property(nonatomic,strong)UITableView *chatList;
@property(nonatomic,strong)NSIndexPath *selectedIndexPath;
@property(nonatomic,strong)NSFetchedResultsController *resultController;
@property(nonatomic,strong)NSArray *data;

@end

@implementation SSChatMainController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initRightBarButton];
    [[GMCXMPPManager standardDefault] addPersonalInfoDelegate:self];
    [self initUI];
    [self initConstraints];
    NSError *error = nil;
    [self.resultController performFetch:&error];
    self.data = self.resultController.fetchedObjects;
    [self.chatList reloadData];
}

- (void)initRightBarButton {
    UIButton *addBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"chat_contacts"] forState:0];
        [btn addTarget:self action:@selector(showContacts) forControlEvents:UIControlEventTouchUpInside];
        btn;
    });
    [addBtn sizeToFit];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)showContacts {
    [[GMCRoute shared] jumpList:@"Contacts" data:nil source:self];
}

- (void)cardRefreshed {
    [self.chatList reloadData];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    self.data = controller.fetchedObjects;
    [self.chatList reloadData];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.selectedIndexPath) {
        SSChatListCell *cell = [self.chatList cellForRowAtIndexPath:self.selectedIndexPath];
        cell.selected = false;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SSChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:[SSChatListCell getIdentify] forIndexPath:indexPath];
    cell.object = self.data[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    XMPPMessageArchiving_Contact_CoreDataObject *object = self.data[indexPath.row];
    NSString *name = object.bareJidStr;
    if ([name containsString:userDomainMore]) {
        name = [name stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
    }
//    else if ([name containsString:@"@private.embrace-changed"]) {
//        name = [name stringByReplacingOccurrencesOfString:@"@private.embrace-changed" withString:@""];
//    }
    SSChatListCell *cell = (SSChatListCell *)[tableView cellForRowAtIndexPath:indexPath];
    [[GMCRoute shared] jumpList:@"SSChat" data:@{@"name":name, @"jidBare":object.bareJidStr, @"isGroup": @(cell.isGroup)} source:self];
}

#pragma mark - TODO 删除会话
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    XMPPMessageArchiving_Contact_CoreDataObject *object = self.data[indexPath.row];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext deleteObject:object];
        [[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext save:nil];
    }
}

#pragma mark - 设置界面
- (void)initUI {
    self.chatList = ({
        UITableView *table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        table.delegate = self;
        table.dataSource = self;
        [table registerClass:[SSChatListCell class] forCellReuseIdentifier:[SSChatListCell getIdentify]];
        table.tableFooterView = [UIView new];
        [self.view addSubview:table];
        table;
    });
}

- (void)initConstraints {
    
}

- (NSFetchedResultsController *)resultController {
    if (!_resultController) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Contact_CoreDataObject" inManagedObjectContext:[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext];
//        request.predicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@", [GMCXMPPManager standardDefault].xmppStream.myJID.bareJID];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"mostRecentMessageTimestamp" ascending:NO];
        request.sortDescriptors = @[sort];
        _resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
        _resultController.delegate = self;
    }
    return _resultController;
}

@end
