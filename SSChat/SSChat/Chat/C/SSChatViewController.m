//
//  SSChatViewController.m
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSChatViewController.h"
#import "GMCRoute.h"
#import "GMCInputTool.h"
#import "SSTextMessageCell.h"
#import "Fragmentary.h"
#import "UIImage+SSImage.h"
#import "SSChatMessageModel.h"
#import <Masonry/Masonry.h>
#import "GMCImagePicker.h"
#import "GMCXMPPManager.h"
#import "SSImageMessageCell.h"
#import "GMCMPMManager.h"

@interface SSChatViewController ()<UITableViewDelegate, UITableViewDataSource, GMCInputToolDelegate, UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate, GMCXMPPManagerPersonalInfoProtocol>
@property(nonatomic,strong)UIImageView *bgImageView;
@property(nonatomic,strong)UITableView *messageList;
@property(nonatomic,strong)NSMutableArray *dataArray;
@property(nonatomic,strong)GMCInputTool *tool;
@property(nonatomic,strong)NSFetchedResultsController *resultController;
@property(nonatomic,strong)NSString *jidBare;
@property(nonatomic,assign)BOOL isGroup;

@end

@implementation SSChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self initRightBarButton];
    [[GMCXMPPManager standardDefault] addPersonalInfoDelegate:self];
    [self initUI];
    [self initConstraints];
//    [self mock];
    NSDictionary *dict = [[GMCRoute shared] getDataForSource:self];
    self.title = [dict valueForKey:@"name"];
    self.jidBare = [dict valueForKey:@"jidBare"];
    self.isGroup = [dict valueForKey:@"isGroup"];
    self.navigationController.navigationBar.translucent = NO;
    [self.resultController performFetch:nil];
    self.dataArray = self.resultController.fetchedObjects.mutableCopy;
    [self.messageList reloadData];
}

- (void)initRightBarButton {
    UIButton *addBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"contacts_add_friend"] forState:0];
        [btn addTarget:self action:@selector(joinRoom) forControlEvents:UIControlEventTouchUpInside];
        btn;
    });
    [addBtn sizeToFit];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)joinRoom {
//    NSString *user = self.jidBare;
//    if ([self.jidBare containsString:@"@embrace-changed"]) {
//        user = [user stringByReplacingOccurrencesOfString:@"@embrace-changed" withString:@""];
//        user = [NSString stringWithFormat:@"%@-聊天室", user];
//    }
//    [[GMCMPMManager standardDefault] joinRoom:user nickName:@"我是大娃"];
}


- (void)cardRefreshed {
    [self.messageList reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self messageListScrollToBottom];
}

#pragma mark - 设置界面
- (void)initUI {
    UIImage *bgImage = [UIImage imageWithColor:UIColor.whiteColor size:self.view.bounds.size];
    self.bgImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:bgImage];
        imageView.frame = self.view.bounds;
        imageView.backgroundColor = UIColor.blueColor;
        [self.view addSubview:imageView];
        imageView;
    });
    
    self.messageList = ({
        UITableView *table = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        table.delegate = self;
        table.dataSource = self;
        [table registerClass:[SSTextMessageCell class] forCellReuseIdentifier:[SSTextMessageCell getIdentify]];
        [table registerClass:[SSImageMessageCell class] forCellReuseIdentifier:[SSImageMessageCell getIdentify]];
        table.backgroundColor = UIColor.clearColor;
        table.estimatedRowHeight = 200;
        table.rowHeight = UITableViewAutomaticDimension;
        table.contentInset = UIEdgeInsetsZero;
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] init];
        tapGesture.delegate = self;
        [table addGestureRecognizer:tapGesture];
        [self.view addSubview:table];
        table;
    });
    self.tool = [[GMCInputTool alloc] init];
    self.tool.delegate = self;
    [self.view addSubview:self.tool];
}

- (void)initConstraints {
    [self.tool mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.leading.trailing.equalTo(self.view);
        make.height.mas_equalTo(NavigationBarHeight);
    }];
    
    [self.messageList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(self.view);
        make.bottom.equalTo(self.tool.mas_top).offset(-5);
    }];
}

- (void)moreViewClick:(NSInteger)index {
    GMCImagePicker *picker = [GMCImagePicker sharedManager];
    [picker imagePickerCompletionAtType:GMCImagePickerStyleDefault needSheet:YES ViewController:self Completion:^(UIImage * _Nonnull finalImage) {
        NSString *jid = self.jidBare;
        if ([self.jidBare containsString:userDomainMore]) {
            jid = [jid stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
        }
        [[GMCXMPPManager standardDefault] sendMessage:finalImage type:@"chat" jid:jid];
    }];
}

- (void)sendClick:(NSString *)message {
    NSString *jid = self.jidBare;
    if ([self.jidBare containsString:userDomainMore]) {
        jid = [jid stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
    }
//    else if ([self.jidBare containsString:@"@private.embrace-changed"]) {
//        jid = [jid stringByReplacingOccurrencesOfString:@"@private.embrace-changed" withString:@""];
//    }
    NSString *type = @"chat";
//    if (_isGroup) {
//        type = @"groupchat";
//    }
    [[GMCXMPPManager standardDefault] sendMessage:message type:type jid:jid];
}

- (void)facesClick:(NSObject *)obj {
    NSLog(@"");
}


- (void)tableViewShouldChange {
    [self messageListScrollToBottom];
}

- (void)messageListScrollToBottom {
    if (self.dataArray.count > 0) {
        [self.messageList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.dataArray.count - 1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    [self.tool endEditing:YES];
    [self.tool hideAll];
    if([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    XMPPMessageArchiving_Message_CoreDataObject *object = self.dataArray[indexPath.row];
    if ([object.message.subject isEqualToString:@"FILE"]) {
        SSImageMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[SSImageMessageCell getIdentify] forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.object = object;
        cell.iconButtonClick = ^{
            [self goToPersonal];
        };
        return cell;
    } else {
        SSTextMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:[SSTextMessageCell getIdentify] forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.object = object;
        cell.iconButtonClick = ^{
            if (object.isOutgoing) {
                [self goToPersonal];
            } else {
                
            }
            
        };
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
}


- (void)goToPersonal {
    [[GMCRoute shared] jumpList:@"Personal" data:@{@"name":@"123", @"jidBare":@"123"} source:self];
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    NSArray *temp = controller.fetchedObjects.mutableCopy;
    if (temp.count) {
        [self.dataArray addObject:temp.lastObject];
        [self.messageList reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.dataArray.count - 1 inSection:0];
        [self.messageList scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
}

- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (NSFetchedResultsController *)resultController {
    if (!_resultController) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext];
        request.predicate = [NSPredicate predicateWithFormat:@"bareJidStr=%@", self.jidBare];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
        request.sortDescriptors = @[sort];
        _resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[XMPPMessageArchivingCoreDataStorage sharedInstance].mainThreadManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
        _resultController.delegate = self;
    }
    return _resultController;
}

@end
