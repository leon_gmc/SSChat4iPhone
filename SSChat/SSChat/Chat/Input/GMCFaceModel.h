//
//  GMCFaceModel.h
//  SSChat
//
//  Created by Gmc on 2019/6/16.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMCFaceModel : NSObject

@property(nonatomic,strong)NSString *code;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *imageName;
+ (GMCFaceModel *)entityWithDictionary:(NSDictionary*)dic atIndex:(int)index;

/// 通过facename获取对应imagename
/// @param faceName facename
+ (nullable NSString *)getFaceImageName:(NSString *)faceName;

@end

NS_ASSUME_NONNULL_END
