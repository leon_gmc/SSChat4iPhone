//
//  GMCFaceModel.m
//  SSChat
//
//  Created by Gmc on 2019/6/16.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCFaceModel.h"

@implementation GMCFaceModel


+ (GMCFaceModel *)entityWithDictionary:(NSDictionary*)dic atIndex:(int)index {
    GMCFaceModel* entity = [[GMCFaceModel alloc] init];
    entity.name = dic[@"name"];
    entity.code = [NSString stringWithFormat:@"[%d]", index];
    entity.imageName = [NSString stringWithFormat:@"Expression_%d.png", index+1];
    return entity;
}


+ (nullable NSString *)getFaceImageName:(NSString *)faceName {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"faces_icons.plist" ofType:nil];
    NSArray* array = [NSArray arrayWithContentsOfFile:path];
    for(int i = 0; i < array.count; i ++){
        NSDictionary *map = array[i];
        if ([map.allValues containsObject:faceName]) {
            return [NSString stringWithFormat:@"Expression_%d.png", i+1];
        }
    }
    return nil;
}

@end
