//
//  GMCFaceView.h
//  SSChat
//
//  Created by Gmc on 2019/5/30.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GMCFaceViewProtocol <NSObject>

- (void)facesClick:(NSObject *)obj;
- (void)faceSendClick;
- (void)facesDeleteClick;
@end

@interface GMCFaceView : UIView

@property(weak,nonatomic)id<GMCFaceViewProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
