//
//  GMCFaceView.m
//  SSChat
//
//  Created by Gmc on 2019/5/30.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCFaceView.h"
#import <Masonry/Masonry.h>
#import "SSFaceCollectionViewCell.h"
#import "Fragmentary.h"
#import "SSCollectionViewFlowLayout.h"
#import "GMCFaceManager.h"
@interface GMCFaceView ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSArray *emojis;
@property(nonatomic,strong)NSArray *facess;
@property(nonatomic,strong)UIButton *send;

@end

@implementation GMCFaceView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self getEmoji];
        [self initUI];
    }
    return self;
}

- (void)getEmoji {
    self.facess = [[GMCFaceManager shared] getFaceArray];
}

- (void)sendAction {
    if ([self.delegate respondsToSelector:@selector(faceSendClick)]) {
        [self.delegate faceSendClick];
    }
}

#pragma mark - 设置界面
- (void)initUI {
    self.collectionView = ({
        SSCollectionViewFlowLayout *flow = [[SSCollectionViewFlowLayout alloc] init];
        flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat width = (W - 30 - (7 * 10)) / 8;
        flow.size = CGSizeMake(width, width);
        flow.pageWidth = W - 30;
        flow.row = 3;
        flow.column = 8;
        flow.rowSpacing = 10;
        flow.columnSpacing = 10;
        UICollectionView *collection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        collection.pagingEnabled = YES;
        collection.backgroundColor = UIColor.clearColor;
        [collection registerClass:[SSFaceCollectionViewCell class] forCellWithReuseIdentifier:[SSFaceCollectionViewCell getIdentify]];
        collection.dataSource = self;
        collection.delegate = self;
        [self addSubview:collection];
        collection;
    });
    
    self.send = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"发送" forState:UIControlStateNormal];
        [btn setBackgroundColor:UIColor.whiteColor];
        [btn setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    [self initConstraints];
    self.backgroundColor = UIColor.redColor;
}

- (void)initConstraints {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(15);
        make.bottom.equalTo(self).offset(-25);
        make.right.equalTo(self).offset(-15);
    }];
    [self.send mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.top.equalTo(self.collectionView.mas_bottom);
        make.right.equalTo(self.collectionView);
        make.width.mas_equalTo(50);
    }];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.facess.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SSFaceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[SSFaceCollectionViewCell getIdentify] forIndexPath:indexPath];
    cell.entity = self.facess[indexPath.item];
    cell.deleteblock = ^{
        if ([self.delegate respondsToSelector:@selector(facesDeleteClick)]) {
            [self.delegate facesDeleteClick];
        }
    };
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSObject *obj = self.facess[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(facesClick:)]) {
        [self.delegate facesClick:obj];
    }
}

@end
