//
//  GMCInputTool.h
//  SSChat
//
//  Created by Gmc on 2019/5/21.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@protocol GMCInputToolDelegate <NSObject>

- (void)tableViewShouldChange;

- (void)moreViewClick:(NSInteger)index;

- (void)facesClick:(NSObject *)obj;

- (void)sendClick:(NSString *)message;

@end

@interface GMCInputTool : UIView

@property(nonatomic,weak) id<GMCInputToolDelegate>delegate;

- (void)hideAll;

@end

NS_ASSUME_NONNULL_END
