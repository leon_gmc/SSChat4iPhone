//
//  GMCInputTool.m
//  SSChat
//
//  Created by Gmc on 2019/5/21.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCInputTool.h"
#import <Masonry/Masonry.h>
#import "GMCTextView.h"
#import "GMCMoreView.h"
#import "GMCFaceView.h"
#import "Fragmentary.h"
#import "GMCFaceModel.h"

static float textHeight = 36.0f;
static float boardHeight = 300;
@interface GMCInputTool ()<UITextViewDelegate, GMCMoreViewProtocol, GMCFaceViewProtocol>

@property(nonatomic,strong)GMCTextView *textView;

@property(nonatomic,strong)UIButton *faceButton;
@property(nonatomic,strong)UIButton *moreButton;
@property(nonatomic,assign)CGFloat keyboardHeight;
@property(nonatomic,assign)CGRect originFrame;
@property(nonatomic,assign)CGFloat oldHeight;
@property(nonatomic,strong)GMCFaceView *faceView;
@property(nonatomic,strong)GMCMoreView *moreView;
@property(nonatomic,copy)NSString * faceString;


@end

@implementation GMCInputTool

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self ListenNotification];
    }
    return self;
}

- (void)moreViewClick:(NSInteger)index {
    if ([self.delegate respondsToSelector:@selector(moreViewClick:)]) {
        [self.delegate moreViewClick:index];
    }
}

- (void)faceSendClick {
    if ([self.delegate respondsToSelector:@selector(sendClick:)]) {
        [self.delegate sendClick:self.faceString];
        self.textView.text = @"";
    }
}

- (void)facesDeleteClick {
    [self.textView deleteBackward];
}

- (void)facesClick:(NSObject *)obj {
    if ([obj isKindOfClass:[GMCFaceModel class]]) {
        GMCFaceModel *model = (GMCFaceModel *)obj;
        self.textView.text = [self.textView.text stringByAppendingString:model.name];
        [self.textView.delegate textViewDidChange:self.textView];
        self.faceString = self.textView.text;
    }
    
    
    if ([self.delegate respondsToSelector:@selector(facesClick:)]) {
        [self.delegate facesClick:obj];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.faceButton.selected = NO;
    self.moreButton.selected = NO;
    [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_nor"] forState:UIControlStateNormal];
    [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_press"] forState:UIControlStateHighlighted];
    [UIView animateWithDuration:0.25 animations:^{
        [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.textView.mas_bottom).offset(H);
        }];
        [self.faceView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.textView.mas_bottom).offset(H);
        }];
        [self.superview layoutIfNeeded];
    }];
}



- (void)hideAll {
    if (self.moreButton.isSelected || self.faceButton.isSelected) {
        [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_nor"] forState:UIControlStateNormal];
        [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_press"] forState:UIControlStateHighlighted];
        self.moreButton.selected = NO;
        self.faceButton.selected = NO;
        self.moreButton.enabled = NO;
        self.faceButton.enabled = NO;
        [UIView animateWithDuration:0.25 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(CGRectGetHeight(self.textView.bounds) + 40);
            }];
            [self.faceView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.superview layoutIfNeeded];
        } completion:^(BOOL finished) {
            if (finished) {
                self.moreButton.enabled = YES;
                self.faceButton.enabled = YES;
            }
        }];
    }
}

- (void)ListenNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self initUI];   
}

- (void)keyboardWillShow:(NSNotification *)noti {
    NSDictionary *userInfo = noti.userInfo;
    CGRect endFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration delay:0 options:[noti.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue] animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(20 + endFrame.size.height + CGRectGetHeight(self.textView.bounds)));
        }];
        [self.superview layoutIfNeeded];
        [self changeTableViewFrame];
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)noti {
    if (self.moreButton.isSelected || self.faceButton.isSelected) {
        return;
    }
    NSDictionary *userInfo = noti.userInfo;
    CGFloat duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration delay:0 options:[noti.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue] animations:^{
        [self mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(CGRectGetHeight(self.textView.bounds) + 40));
        }];
        [self.superview layoutIfNeeded];
    } completion:nil];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        if ([self.delegate respondsToSelector:@selector(sendClick:)]) {
            [self.delegate sendClick:textView.text];
        }
        self.textView.text = @"";
        [self textViewDidChange:textView];
        return NO;
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    CGFloat textInputHeight = ceilf([self.textView sizeThatFits:CGSizeMake(self.textView.bounds.size.width, MAXFLOAT)].height);
    if (textInputHeight > self.textView.maxTextH) {
        self.textView.scrollEnabled = YES;
        [self changeFrame:self.textView.maxTextH];
    } else {
        self.textView.scrollEnabled = NO;
        [self changeFrame:textInputHeight];
    }
    [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length, 1)];
    [self changeTableViewFrame];
    
}

- (void)changeFrame:(CGFloat)height {
    CGFloat h = height - self.textView.frame.size.height;
    [self.textView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(height));
    }];
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(self.frame.size.height + h);
    }];
}

- (void)changeTableViewFrame {
    if ([self.delegate respondsToSelector:@selector(tableViewShouldChange)]) {
        [self.delegate performSelector:@selector(tableViewShouldChange)];
    }
}

- (void)moreBtn:(UIButton *)sender {
    self.faceButton.selected = NO;
    [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_nor"] forState:UIControlStateNormal];
    [self.faceButton setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_press"] forState:UIControlStateHighlighted];
    sender.selected = !sender.selected;
    if (!self.moreView.superview) {
        [self addSubview:self.moreView];
    }
    if (sender.selected) {
        [self endEditing:YES];
        [UIView animateWithDuration:0.25 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(CGRectGetHeight(self.textView.bounds) + CGRectGetHeight(self.moreView.bounds) + 20);
            }];
            [self.superview layoutIfNeeded];
        }];
        [UIView animateWithDuration:0.25 animations:^{
            [self.faceView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.superview layoutIfNeeded];
        }];
        [UIView animateWithDuration:0.25 animations:^{
            [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom);
            }];
            [self.superview layoutIfNeeded];
        }];
        [self changeTableViewFrame];
    } else {
        [self.textView becomeFirstResponder];
        [UIView animateWithDuration:0.25 animations:^{
            [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
               make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.superview layoutIfNeeded];
        }];
    }
    
}

- (void)faceBtn:(UIButton *)sender {
    self.moreButton.selected = NO;
    sender.selected = !sender.selected;
    if (sender.selected) {
        [self endEditing:YES];
        [sender setBackgroundImage:[UIImage imageNamed:@"chat_bottom_board_nor"] forState:UIControlStateNormal];
        [sender setBackgroundImage:[UIImage imageNamed:@"chat_bottom_board_press"] forState:UIControlStateHighlighted];
        [UIView animateWithDuration:0.25 animations:^{
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.mas_equalTo(CGRectGetHeight(self.textView.bounds) + CGRectGetHeight(self.faceView.bounds) + 20);
            }];
            [self.superview layoutIfNeeded];
        }];
        [UIView animateWithDuration:0.25 animations:^{
            [self.moreView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.superview layoutIfNeeded];
        }];
        [UIView animateWithDuration:0.25 animations:^{
            [self.faceView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom);
            }];
            [self.superview layoutIfNeeded];
        }];
        
        [self changeTableViewFrame];
    } else {
        [self.textView becomeFirstResponder];
        [UIView animateWithDuration:0.25 animations:^{
            [self.faceView mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.textView.mas_bottom).offset(H);
            }];
            [self.superview layoutIfNeeded];
        }];
        
    }
}

#pragma mark - 设置界面
- (void)initUI {
    self.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1.00];
    self.textView = ({
        GMCTextView *textV = [[GMCTextView alloc] init];
        textV.delegate = self;
        textV.font = [UIFont systemFontOfSize:16];
        [textV textValueDidChanged:^(CGFloat textHeight) {
            [self changeFrame:textHeight];
        }];
        textV.maxNumberOfLines = 5;
        textV.layer.cornerRadius = 4;
        textV.layer.borderWidth = 1;
        textV.layer.borderColor = [UIColor colorWithRed:0.88 green:0.88 blue:0.89 alpha:1.00].CGColor;
        [self addSubview:textV];
        textV;
    });
    
    self.moreButton = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[UIImage imageNamed:@"chat_bottom_more_nor"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"chat_bottom_more_press"] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(moreBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    
    self.faceButton = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_nor"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"chat_bottom_face_press"] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(faceBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        btn;
    });
    self.originFrame = self.frame;
    [self initConstraints];
}

- (void)initConstraints {
    
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self).offset(-5);
        make.top.equalTo(self).offset(10);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    
    [self.faceButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(self.moreButton.mas_leading).offset(-7);
        make.top.equalTo(self.moreButton);
        make.size.equalTo(self.moreButton);
    }];
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self).offset(15);
        make.top.equalTo(self).offset(5);
        make.right.equalTo(self.faceButton.mas_left).offset(-5);
        make.height.equalTo(@(textHeight));
    }];
    
    [self.moreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(H);
        make.left.right.equalTo(self);
        make.height.equalTo(@(boardHeight - NavigationBarHeight - 10));
    }];
    
    [self.faceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textView.mas_bottom).offset(H);
        make.left.right.equalTo(self);
        make.height.equalTo(@(boardHeight - NavigationBarHeight - 10));
//        make.bottom.equalTo(self).offset(-10);
    }];
}

- (GMCFaceView *)faceView {
    if (!_faceView) {
        _faceView = [[GMCFaceView alloc] init];
        _faceView.backgroundColor = UIColor.clearColor;
        _faceView.delegate = self;
        [self addSubview:_faceView];
    }
    return _faceView;
}

- (GMCMoreView *)moreView {
    if (!_moreView) {
        _moreView = [[GMCMoreView alloc] init];
        _moreView.delegate = self;
        [self addSubview:_moreView];
    }
    return _moreView;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
