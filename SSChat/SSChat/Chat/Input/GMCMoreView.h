//
//  GMCMoreView.h
//  SSChat
//
//  Created by Gmc on 2019/5/30.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GMCMoreViewProtocol <NSObject>

- (void)moreViewClick:(NSInteger)index;

@end

@interface GMCMoreView : UIView

@property(weak,nonatomic)id<GMCMoreViewProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
