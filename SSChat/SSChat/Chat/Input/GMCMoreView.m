//
//  GMCMoreView.m
//  SSChat
//
//  Created by Gmc on 2019/5/30.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCMoreView.h"
#import "SSCollectionViewFlowLayout.h"
#import "Fragmentary.h"
#import "SSMoreViewCollectionViewCell.h"
#import <Masonry/Masonry.h>

@interface GMCMoreView ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic,strong)UICollectionView *collectionView;

@end

@implementation GMCMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SSMoreViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[SSMoreViewCollectionViewCell getIdentify] forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(moreViewClick:)]) {
        [self.delegate moreViewClick:indexPath.row];
    }
}

#pragma mark - 设置界面
- (void)initUI {
    self.collectionView = ({
        SSCollectionViewFlowLayout *flow = [[SSCollectionViewFlowLayout alloc] init];
        flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        CGFloat width = ((W - 30) - 3 * 40) / 4;
        flow.size = CGSizeMake(width, width);
        flow.pageWidth = W - 30;
        flow.row = 2;
        flow.column = 4;
        flow.rowSpacing = 40;
        flow.columnSpacing = 40;
        UICollectionView *collection = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flow];
        collection.pagingEnabled = YES;
        collection.backgroundColor = UIColor.clearColor;
        [collection registerClass:[SSMoreViewCollectionViewCell class] forCellWithReuseIdentifier:[SSMoreViewCollectionViewCell getIdentify]];
        collection.dataSource = self;
        collection.delegate = self;
        [self addSubview:collection];
        collection;
    });
}

- (void)initConstraints {
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(15);
        make.bottom.equalTo(self).offset(-5);
        make.right.equalTo(self).offset(-15);
    }];
}

@end
