//
//  SSFaceCollectionViewCell.h
//  SSChat
//
//  Created by Gmc on 2019/6/2.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseCollectionViewCell.h"
@class GMCFaceModel;
typedef void(^cellblock)(void);

NS_ASSUME_NONNULL_BEGIN

@interface SSFaceCollectionViewCell : SSBaseCollectionViewCell

@property(nonatomic,copy)GMCFaceModel *entity;

@property(nonatomic,copy)cellblock deleteblock;

@end

NS_ASSUME_NONNULL_END
