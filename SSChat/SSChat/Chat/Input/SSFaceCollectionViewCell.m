//
//  SSFaceCollectionViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/6/2.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSFaceCollectionViewCell.h"
#import "GMCFaceModel.h"
#import <Masonry/Masonry.h>

@interface SSFaceCollectionViewCell ()

@property(nonatomic,strong)UIImageView *facesImageView;
@property(nonatomic,strong)UIButton *delete;

@end



@implementation SSFaceCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

- (void)deleteAction {
    self.deleteblock ? self.deleteblock() : nil;
}

#pragma mark - 设置界面
- (void)initUI {
    self.delete = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundImage:[UIImage imageNamed:@"DeleteEmoticonBtn"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"DeleteEmoticonBtnHL"] forState:UIControlStateHighlighted];
        [btn addTarget:self action:@selector(deleteAction) forControlEvents:UIControlEventTouchUpInside];
        btn.hidden = YES;
        btn;
    });
    [self.contentView addSubview:self.delete];
    self.facesImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.facesImageView];
    
}

- (void)initConstraints {
    [self.delete mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self.facesImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
}

- (void)setEntity:(GMCFaceModel *)entity {
    _entity = entity;
    if ([entity.name isEqualToString:@"delete_btn"]) {
        self.delete.hidden = NO;
        self.facesImageView.hidden = YES;
    } else {
        self.delete.hidden = YES;
        self.facesImageView.hidden = NO;
        self.facesImageView.image = [UIImage imageNamed:entity.imageName];
    }
}

@end
