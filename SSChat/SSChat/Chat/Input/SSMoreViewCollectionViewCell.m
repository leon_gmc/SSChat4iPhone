//
//  SSMoreViewCollectionViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/6/12.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSMoreViewCollectionViewCell.h"
#import <Masonry/Masonry.h>

@interface SSMoreViewCollectionViewCell ()

@property(nonatomic,strong)UIImageView *picture;


@end

@implementation SSMoreViewCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

#pragma mark - 设置界面
- (void)initUI {
    self.contentView.backgroundColor = UIColor.whiteColor;
    self.contentView.layer.cornerRadius = 4.0;
    self.contentView.layer.masksToBounds = YES;
    self.picture = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"picture"]];
        imageView;
    });
    [self.contentView addSubview:self.picture];
}

- (void)initConstraints {
    [self.picture mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
}

@end
