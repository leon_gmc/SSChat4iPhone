//
//  SSChatMessageModel.h
//  SSChat
//
//  Created by Gmc on 2019/5/24.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSChatMessageModel : SSBaseModel

@property(nonatomic,copy)NSString * type;
@property(nonatomic,copy)NSString * name;
@property(nonatomic,copy)NSString * iconPath;
@property(nonatomic,copy)NSString * message;

@end

NS_ASSUME_NONNULL_END
