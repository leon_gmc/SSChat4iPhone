//
//  SSChatMessageModel.m
//  SSChat
//
//  Created by Gmc on 2019/5/24.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSChatMessageModel.h"

@implementation SSChatMessageModel

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.name = [dict valueForKey:@"name"];
        self.type = [dict valueForKey:@"type"];
        self.iconPath = [dict valueForKey:@"iconPath"];
        self.message = [dict valueForKey:@"message"];
    }
    return self;
}

- (id)valueForUndefinedKey:(NSString *)key {
    return nil;
}

@end
