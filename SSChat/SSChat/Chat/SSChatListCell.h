//
//  SSChatListCell.h
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"
#import "XMPPMessageArchiving_Contact_CoreDataObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSChatListCell : SSBaseTableViewCell
@property(nonatomic,strong)XMPPMessageArchiving_Contact_CoreDataObject *object;
@property(nonatomic,assign, readonly)BOOL isGroup;

@end

NS_ASSUME_NONNULL_END
