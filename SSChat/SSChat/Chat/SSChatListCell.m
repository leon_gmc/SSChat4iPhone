//
//  SSChatListCell.m
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSChatListCell.h"
#import "GMCXMPPManager.h"

@interface SSChatListCell ()

@property(nonatomic,strong)UIImageView *iconImageView;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *shortMsg;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)XMPPJID * jid;
@end

@implementation SSChatListCell
{
    BOOL _isGroup;
}

- (BOOL)isGroup {
    return _isGroup;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

- (void)setRefreshAvatar:(NSString *)refreshAvatar {
    NSData *data = [[GMCXMPPManager standardDefault] getAvatar:self.jid];
    if (data) {
        self.iconImageView.image = [UIImage imageWithData:data];
    } else {
        self.iconImageView.image = [UIImage imageNamed:@"img_defaulthead_nor"];
    }
}

- (void)setObject:(XMPPMessageArchiving_Contact_CoreDataObject *)object {
    _object = object;
    self.jid = object.bareJid;
    NSString *name = object.bareJidStr;
    
    if ([name containsString:userDomainMore]) {
        name = [name stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
        _isGroup = NO;
    }
//    else if ([name containsString:@"@private.embrace-changed"]) {
//        name = [name stringByReplacingOccurrencesOfString:@"@private.embrace-changed" withString:@""];
//        _isGroup = YES;
//    }
    self.nameLabel.text = name;
    if ([object.mostRecentMessageBody containsString:@"jpg"] || [object.mostRecentMessageBody containsString:@"png"]) {
        self.shortMsg.text = @"[图片]";
    } else {
        self.shortMsg.text = object.mostRecentMessageBody;
    }
    NSDate *date = object.mostRecentMessageTimestamp;
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    formatter.dateFormat = @"HH:mm";
    NSString *dateStr = [formatter stringFromDate:date];
    self.timeLabel.text = dateStr;
    NSData *data = [[GMCXMPPManager standardDefault] getAvatar:object.bareJid];
    if (data) {
        self.iconImageView.image = [UIImage imageWithData:data];
    } else {
        if (_isGroup) {
            self.iconImageView.image = [UIImage imageNamed:@"img_defaulthead_nor"];
        } else {
            self.iconImageView.image = [UIImage imageNamed:@"img_defaulthead_nor"];
        }
        
    }
}

- (void)initUI {
    self.iconImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_defaulthead_nor"]];
//        imageView.layer.cornerRadius = 4.0;
//        imageView.layer.masksToBounds = YES;
        [self.contentView addSubview:imageView];
        imageView;
    });
    
    self.nameLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.text = @"Leon";
        label.textColor = UIColor.blackColor;
        label.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:label];
        label;
    });
    
    self.shortMsg = ({
        UILabel *label = [[UILabel alloc] init];
        label.text = @"我吃鸡绝对牛逼";
        label.textColor = UIColor.grayColor;
        label.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:label];
        label;
    });
    
    self.timeLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.text = @"9:00 PM";
        label.textColor = UIColor.grayColor;
        label.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:label];
        label;
    });
}

- (void)initConstraints {
    self.iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *iconH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[_iconImageView(==44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView)];
    NSArray *iconV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_iconImageView(==44)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView)];
    [self.contentView addConstraints:iconH];
    [self.contentView addConstraints:iconV];
    
    self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *nameH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_iconImageView]-10-[_nameLabel(<=200)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView, _nameLabel)];
    NSArray *nameV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_nameLabel]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_nameLabel)];
    [self.contentView addConstraints:nameH];
    [self.contentView addConstraints:nameV];
    
    self.shortMsg.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *shortMsgH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_iconImageView]-10-[_shortMsg(<=200)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView, _shortMsg)];
    NSArray *shortMsgV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_shortMsg]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView, _shortMsg)];
    [self.contentView addConstraints:shortMsgH];
    [self.contentView addConstraints:shortMsgV];
    
    self.timeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *timeH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_timeLabel]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_timeLabel)];
    NSArray *timeV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_timeLabel]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_nameLabel, _timeLabel)];
    [self.contentView addConstraints:timeH];
    [self.contentView addConstraints:timeV];
}


@end
