//
//  SSCollectionViewFlowLayout.h
//  SSChat
//
//  Created by Gmc on 2019/6/4.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSCollectionViewFlowLayout : UICollectionViewFlowLayout

@property(nonatomic,assign)NSInteger row;
@property(nonatomic,assign)NSInteger column;
@property(nonatomic,assign)CGFloat rowSpacing;
@property(nonatomic,assign)CGFloat columnSpacing;
@property(nonatomic,assign)CGSize size;
@property(nonatomic,assign)CGFloat pageWidth;

@end

NS_ASSUME_NONNULL_END
