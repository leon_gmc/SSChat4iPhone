//
//  SSCollectionViewFlowLayout.m
//  SSChat
//
//  Created by Gmc on 2019/6/4.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSCollectionViewFlowLayout.h"

@interface SSCollectionViewFlowLayout ()
@property(nonatomic,strong)NSMutableArray *attributesArray;

@end

@implementation SSCollectionViewFlowLayout

- (void)prepareLayout {
    [super prepareLayout];
    self.attributesArray = @[].mutableCopy;
    if (self.pageWidth == 0) {
        self.pageWidth = [UIScreen mainScreen].bounds.size.width;
    }
    self.itemSize = self.size;
    self.minimumLineSpacing = self.rowSpacing;
    self.minimumInteritemSpacing = self.columnSpacing;
    NSUInteger count = [self.collectionView numberOfItemsInSection:0];
    for (NSUInteger i = 0; i < count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        UICollectionViewLayoutAttributes *attributes = [self layoutAttributesForItemAtIndexPath:indexPath];
        [self.attributesArray addObject:attributes];
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *attribute = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    NSInteger index = indexPath.item;
    NSInteger page = index / (self.row * self.column);
     CGFloat x = index % self.column * (self.size.width + self.columnSpacing) + page * self.pageWidth;
    CGFloat y = (index / self.column % self.row) * (self.size.height + self.rowSpacing);
    attribute.frame = CGRectMake(x, y, self.size.width, self.size.height);
    return attribute;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    return self.attributesArray;
}

- (CGSize)collectionViewContentSize{
    int width = (int)ceil(self.attributesArray.count / (self.row * self.column * 1.0)) * self.pageWidth;
    return CGSizeMake(width, 0);
}


@end
