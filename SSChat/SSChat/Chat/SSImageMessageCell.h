//
//  SSImageMessageCell.h
//  SSChat
//
//  Created by Gmc on 2019/8/12.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSImageMessageCell : SSBaseTableViewCell
@property(nonatomic,strong)XMPPMessageArchiving_Message_CoreDataObject *object;
@property(nonatomic,copy)void(^iconButtonClick)(void);
@end

NS_ASSUME_NONNULL_END
