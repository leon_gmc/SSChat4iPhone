//
//  SSImageMessageCell.m
//  SSChat
//
//  Created by Gmc on 2019/8/12.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSImageMessageCell.h"
#import <Masonry/Masonry.h>
#import "GMCXMPPManager.h"

@interface SSImageMessageCell ()
@property(nonatomic,strong)UIImageView *customerImageView;
@property(nonatomic,strong)UIButton *iconButton;

@end

@implementation SSImageMessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setObject:(XMPPMessageArchiving_Message_CoreDataObject *)object {
    _object = object;
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:_object.body];;
    NSData *data = [[NSData alloc] initWithContentsOfFile:path];
    UIImage *image = [UIImage imageWithData:data];
    self.customerImageView.image = image;
//    self.customerImageView.contentMode = UIViewContentModeScaleAspectFit;
//    CGFloat width = 0;
//    if (image.size.height > 160) {
//        width = image.size.width / image.size.height * 160;
//    } else {
//
//    }
    if (_object.isOutgoing) {
        ///send
        [self initSendConstraints:200 width:160];
        NSDictionary *map = [[GMCXMPPManager standardDefault] getPersonalData];
        NSData *iconData = [map valueForKey:@"iamgeData"];
        if (iconData) {
            UIImage *icon = [UIImage imageWithData:iconData];
            [self.iconButton setImage:icon forState:0];
        } else {
            [self.iconButton setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        }
    } else {
        ///recive
        NSData *icondata = [[GMCXMPPManager standardDefault] getAvatar:object.bareJid];
        if (icondata) {
            UIImage *icon = [UIImage imageWithData:icondata];
            [self.iconButton setImage:icon forState:0];
        } else {
            [self.iconButton setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        }
        [self initReciveConstraints:200 width:160];
    }
}

- (void)iconAction:(UIButton *)sender {
    self.iconButtonClick ? self.iconButtonClick() : nil;
}

- (void)initUI {
    self.iconButton = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        [btn setHighlighted:NO];
        [btn addTarget:self action:@selector(iconAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        btn;
    });
    
    self.customerImageView = ({
        UIImageView *imageV = [[UIImageView alloc] init];
        [self.contentView addSubview:imageV];
        imageV;
    });
}
- (void)initReciveConstraints:(CGFloat)height width:(CGFloat)width {
    
    [self.iconButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(38);
        make.height.mas_equalTo(38);
        make.leading.mas_equalTo(self.contentView).offset(15);
        make.top.mas_equalTo(self.contentView).offset(10);
    }];
    
    [self.customerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconButton);
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.iconButton.mas_right).offset(15);
        make.size.mas_equalTo((CGSize){width, height});
    }];
}

- (void)initSendConstraints:(CGFloat)height width:(CGFloat)width {
    
    [self.iconButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(38);
        make.height.mas_equalTo(38);
        make.trailing.mas_equalTo(self.contentView).offset(-15);
        make.top.mas_equalTo(self.contentView).offset(10);
    }];
    
    [self.customerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconButton);
        make.right.equalTo(self.iconButton.mas_left).offset(-15);
        make.bottom.equalTo(self.contentView);
        make.size.mas_equalTo((CGSize){width, height});
    }];
}

@end
