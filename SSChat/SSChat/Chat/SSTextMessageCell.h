//
//  SSTextMessageCell.h
//  SSChat
//
//  Created by Gmc on 2019/5/21.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"
#import "SSChatMessageModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSTextMessageCell : SSBaseTableViewCell

@property(nonatomic,strong)SSChatMessageModel *model;
@property(nonatomic,strong)XMPPMessageArchiving_Message_CoreDataObject *object;
@property(nonatomic,copy)void(^iconButtonClick)(void);

@end

NS_ASSUME_NONNULL_END
