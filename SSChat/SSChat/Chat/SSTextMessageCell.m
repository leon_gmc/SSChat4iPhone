//
//  SSTextMessageCell.m
//  SSChat
//
//  Created by Gmc on 2019/5/21.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSTextMessageCell.h"
#import <Masonry/Masonry.h>
#import "GMCXMPPManager.h"
#import "GMCFaceModel.h"

@interface SSTextMessageCell ()

@property(nonatomic,strong)UIButton *iconButton;
@property(nonatomic,strong)UIImageView *bubble;
@property(nonatomic,strong)UILabel *message;



@end

@implementation SSTextMessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)setObject:(XMPPMessageArchiving_Message_CoreDataObject *)object {
    _object = object;
    if (object.isOutgoing) {
        /// send
        self.bubble.image = [UIImage imageNamed:@"sendBubble"];
        [self initSendContraints];
        NSDictionary *map = [[GMCXMPPManager standardDefault] getPersonalData];
        NSData *iconData = [map valueForKey:@"iamgeData"];
        if (iconData) {
            UIImage *icon = [UIImage imageWithData:iconData];
            [self.iconButton setImage:icon forState:0];
        } else {
            [self.iconButton setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        }
    } else {
        ///recive
        self.bubble.image = [UIImage imageNamed:@"reciveBubble"];
        [self initReciveConstraints];
        NSData *data = [[GMCXMPPManager standardDefault] getAvatar:object.bareJid];
        if (data) {
            UIImage *icon = [UIImage imageWithData:data];
            [self.iconButton setImage:icon forState:0];
        } else {
            [self.iconButton setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        }
    }
    NSString *message = object.message.body;
    
    if (message) {
        NSString *facePattern = @"\\[[a-zA-Z\\u4e00-\\u9fa5]+\\]";
         NSRegularExpression *regular = [[NSRegularExpression alloc] initWithPattern:facePattern options:NSRegularExpressionCaseInsensitive error:nil];
        NSArray *results = [regular matchesInString:message options:0 range:NSMakeRange(0, message.length)];
        self.message.text = message;
        [results enumerateObjectsUsingBlock:^(NSTextCheckingResult *result, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString *faceName = [message substringWithRange:result.range];
            NSString *imageName = [GMCFaceModel getFaceImageName:faceName];
            if (imageName) {
                //                message = [message stringByReplacingCharactersInRange:result.range withString:@"🐶"];
                NSMutableAttributedString *strM = [[NSMutableAttributedString alloc] initWithAttributedString:self.message.attributedText];
                NSRange newRange = [strM.string rangeOfString:faceName];
                NSTextAttachment *textAttachment = [[NSTextAttachment alloc] init];
                textAttachment.image = [UIImage imageNamed:imageName];
                textAttachment.bounds = CGRectMake(0, -4, self.message.font.lineHeight, self.message.font.lineHeight);
                NSAttributedString *imageText = [NSAttributedString attributedStringWithAttachment:textAttachment];
                [strM replaceCharactersInRange:newRange withAttributedString:imageText];
                [strM addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(newRange.location, 1)];
                self.message.attributedText = strM;
            }
        }];
    }
}

- (void)initUI {
    
    self.iconButton = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"img_defaulthead_nor"] forState:UIControlStateNormal];
        [btn setHighlighted:NO];
        [btn addTarget:self action:@selector(iconAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        btn;
    });
    self.bubble = ({
        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"reciveBubble"]];
        [self.contentView addSubview:imgView];
        imgView;
    });
    self.message = ({
        UILabel *label = [[UILabel alloc] init];
        label.text = @"你好";
        label.numberOfLines = 0;
        [self.contentView addSubview:label];
        label;
    });
}

- (void)initReciveConstraints {
    
    [self.iconButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(38);
        make.height.mas_equalTo(38);
        make.leading.mas_equalTo(self.contentView).offset(15);
        make.top.mas_equalTo(self.contentView).offset(10);
    }];
    
    [self.message mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_lessThanOrEqualTo(200);
        make.leading.equalTo(self.iconButton.mas_trailing).offset(20);
        make.top.mas_equalTo(15);
    }];
    
    [self.bubble mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.message).offset(-15);
        make.trailing.equalTo(self.message).offset(15);
        make.top.equalTo(self.message).offset(-5);
        make.bottom.equalTo(self.message).offset(5);
        make.bottom.equalTo(self.contentView);
    }];
    
}

- (void)initSendContraints {
    
    [self.iconButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(38);
        make.height.mas_equalTo(38);
        make.trailing.mas_equalTo(self.contentView).offset(-15);
        make.top.mas_equalTo(self.contentView).offset(10);
    }];
    
    [self.message mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_lessThanOrEqualTo(200);
        make.trailing.equalTo(self.iconButton.mas_leading).offset(-20);
        make.top.mas_equalTo(15);
    }];
    
    [self.bubble mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self.message).offset(-15);
        make.trailing.equalTo(self.message).offset(15);
        make.top.equalTo(self.message).offset(-5);
        make.bottom.equalTo(self.message).offset(5);
        make.bottom.equalTo(self.contentView);
    }];
}

- (void)iconAction:(UIButton *)sender {
    self.iconButtonClick ? self.iconButtonClick() : nil;
}

@end
