//
//  SSAddFriendViewController.m
//  SSChat
//
//  Created by Gmc on 2019/7/20.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSAddFriendViewController.h"
#import <Masonry/Masonry.h>
#import "GMCXMPPManager.h"

@interface SSAddFriendViewController ()<UITextFieldDelegate>
@property(nonatomic,strong)UITextField *inputFiled;
@property(nonatomic,strong)UIButton *addButton;
@property(nonatomic,copy)NSString * inputText;

@end

@implementation SSAddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"添加好友";
    [self initUI];
    [self initConstraints];
}

- (void)addFriendAction {
    [self.view endEditing:YES];
    [[GMCXMPPManager standardDefault] addNewFriend:self.inputText];
//    [self sendMessageToParent:self.inputText];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    self.inputText = textField.text;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    self.addButton.enabled = textField.text.length;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.inputFiled resignFirstResponder];
    return YES;
}

- (void)initUI {
    self.inputFiled = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.placeholder = @"请输入添加人名称";
        filed.delegate = self;
        filed.layer.cornerRadius = 4;
        filed.layer.masksToBounds = YES;
        filed.layer.borderColor = [UIColor colorWithHexString:@"#FF6E7F"].CGColor;
        filed.layer.borderWidth = 1;
        filed.returnKeyType = UIReturnKeyDone;
        CGRect frame = [filed frame];
        frame.size.width = 8.0f;
        [filed setLeftView:[[UIView alloc]initWithFrame:frame]];
        [filed setLeftViewMode:UITextFieldViewModeAlways];
        [self.view addSubview:filed];
        filed;
    });
    
    self.addButton = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"添加" forState:0];
        [btn setTitleColor:[UIColor whiteColor] forState:0];
        [btn setBackgroundImage:[self getImage] forState:0];
        [btn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"B0C4DE"] size:CGSizeMake(160, 50)] forState:UIControlStateDisabled];
        btn.layer.cornerRadius = 4;
        btn.layer.masksToBounds = YES;
        btn.enabled = NO;
        [btn addTarget:self action:@selector(addFriendAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn;
    });
}

- (void)initConstraints {
    [self.inputFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(100);
        make.left.equalTo(self.view).offset(50);
        make.right.equalTo(self.view).offset(-50);
        make.height.equalTo(@(50));
    }];
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inputFiled.mas_bottom).offset(50);
        make.centerX.equalTo(self.inputFiled);
        make.size.mas_equalTo((CGSize){160, 50});
    }];
}

- (UIImage *)getImage {
    UIView *back = [[UIView alloc]initWithFrame:CGRectMake(0, 0, W, H)];
    back.backgroundColor = UIColor.yellowColor;
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors = @[(__bridge id)[UIColor colorWithHexString:@"#FF6E7F"].CGColor, (__bridge id)[UIColor colorWithHexString:@"#BFE9FF"].CGColor];
    gradientLayer.locations = @[@0.5, @1.0];
    gradientLayer.startPoint = CGPointMake(0, 0);
    gradientLayer.endPoint = CGPointMake(1.0, 0);
    gradientLayer.frame = back.frame;
    [back.layer addSublayer:gradientLayer];
    UIGraphicsBeginImageContext(back.bounds.size);
    [back.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
