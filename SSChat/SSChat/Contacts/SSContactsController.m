//
//  SSContactsController.m
//  SSChat
//
//  Created by Gmc on 2019/5/15.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSContactsController.h"
#import "SSContactsTableViewCell.h"
#import "GMCRoute.h"
#import "GMCXMPPManager.h"

@interface SSContactsController ()<UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property(nonatomic,strong)UITableView *chatList;
@property(nonatomic,strong)NSFetchedResultsController *resultController;
@property(nonatomic,strong)NSArray *chatContacts;



@end

@implementation SSContactsController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"联系人";
    [self initRightBarButton];
    [self initUI];
    [self initConstraints];
    [self.resultController performFetch:nil];
    self.chatContacts = self.resultController.fetchedObjects;
}

- (void)initRightBarButton {
    UIButton *addBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:@"contacts_add_friend"] forState:0];
        [btn addTarget:self action:@selector(addFriend) forControlEvents:UIControlEventTouchUpInside];
        btn;
    });
    [addBtn sizeToFit];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    self.chatContacts = controller.fetchedObjects;
    [self.chatList reloadData];
}

- (void)addFriend {
    [[GMCRoute shared] jumpList:@"AddFriend" data:nil source:self];
}

- (void)sendMessageToParent:(NSObject *)object {
    NSLog(@"");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.chatContacts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SSContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[SSContactsTableViewCell getIdentify] forIndexPath:indexPath];
    XMPPUserCoreDataStorageObject *object = self.chatContacts[indexPath.row];
    cell.nickName = object.jidStr;
    cell.avatar = [[GMCXMPPManager standardDefault] getAvatar:object.jid];
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    XMPPUserCoreDataStorageObject *object = self.chatContacts[indexPath.row];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [[GMCXMPPManager standardDefault] removeFriend:object.jid];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    XMPPUserCoreDataStorageObject *object = self.chatContacts[indexPath.row];
    NSString *nick = object.nickname;
    NSString *name = @"";
    if ([object.jidStr containsString:userDomainMore]) {
        name = [object.jidStr stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
    }
    name = (nick.length == 0 ? name : nick);
    [[GMCRoute shared] jumpList:@"SSChat" data:@{@"name":name, @"jidBare":object.jid.bare} source:self];
}

#pragma mark - 设置界面
- (void)initUI {
    self.chatList = ({
        UITableView *table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        table.delegate = self;
        table.dataSource = self;
        [table registerClass:[SSContactsTableViewCell class] forCellReuseIdentifier:[SSContactsTableViewCell getIdentify]];
        [self.view addSubview:table];
        table;
    });
}

- (void)initConstraints {
    
}

- (NSFetchedResultsController *)resultController {
    if (!_resultController) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        request.entity = [NSEntityDescription entityForName:@"XMPPUserCoreDataStorageObject" inManagedObjectContext:[XMPPRosterCoreDataStorage sharedInstance].mainThreadManagedObjectContext];
        request.predicate = [NSPredicate predicateWithFormat:@"subscription = %@", @"both"];
        NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"jidStr" ascending:YES];
        request.sortDescriptors = @[sort];
        _resultController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:[XMPPRosterCoreDataStorage sharedInstance].mainThreadManagedObjectContext sectionNameKeyPath:nil cacheName:nil];
        _resultController.delegate = self;
    }
    return _resultController;
}


@end
