//
//  SSContactsTableViewCell.h
//  SSChat
//
//  Created by Gmc on 2019/7/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSContactsTableViewCell : SSBaseTableViewCell

@property(nonatomic,copy)NSString *nickName;
@property(nonatomic,strong)NSData *avatar;


@end

NS_ASSUME_NONNULL_END
