//
//  SSContactsTableViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/7/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSContactsTableViewCell.h"
#import <Masonry/Masonry.h>

@interface SSContactsTableViewCell ()
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UIImageView *iconImageView;

@end

@implementation SSContactsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

- (void)setNickName:(NSString *)nickName {
    _nickName = nickName;
    NSString *name = _nickName;
    if ([_nickName containsString:userDomainMore]) {
        name = [_nickName stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
    }
    self.nameLabel.text = name;
}

- (void)setAvatar:(NSData *)avatar {
    _avatar = avatar;
    if (avatar.length) {
        self.iconImageView.image = [UIImage imageWithData:_avatar];
    }
}

- (void)initUI {
    self.nameLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.text = @"Leon";
        [self.contentView addSubview:label];
        label;
    });
    
    self.iconImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_defaulthead_nor"]];
        [self.contentView addSubview:imageView];
        imageView;
    });
}

- (void)initConstraints {
    self.iconImageView.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *iconH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[_iconImageView(==44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView)];
    NSArray *iconV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_iconImageView(==44)]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView)];
    [self.contentView addConstraints:iconH];
    [self.contentView addConstraints:iconV];
    
    self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *nameH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[_iconImageView]-10-[_nameLabel(<=100)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_iconImageView, _nameLabel)];
    NSArray *nameV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_nameLabel]-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:NSDictionaryOfVariableBindings( _nameLabel)];
    [self.contentView addConstraints:nameH];
    [self.contentView addConstraints:nameV];
}

@end
