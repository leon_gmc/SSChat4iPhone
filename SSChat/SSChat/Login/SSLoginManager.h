//
//  SSLoginManager.h
//  SSChat
//
//  Created by Gmc on 2019/8/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^loginComplete)(BOOL isSuccess);

@interface SSLoginManager : NSObject

+ (instancetype)standard;

///检测用户是否登陆
- (BOOL)checLogin;
///登录
- (void)showLogin:(loginComplete)complete;
///登出
- (void)logout:(loginComplete)complete;
///获取用户信息
- (NSDictionary *)getUserInfo;

@end

NS_ASSUME_NONNULL_END
