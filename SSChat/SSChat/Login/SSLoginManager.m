//
//  SSLoginManager.m
//  SSChat
//
//  Created by Gmc on 2019/8/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSLoginManager.h"
#import "SSNavController.h"
#import "SSLoginViewController.h"
#import "GMCXMPPManager.h"

@implementation SSLoginManager

+ (instancetype)standard {
    static dispatch_once_t onceToken;
    static SSLoginManager *_instance = nil;
    dispatch_once(&onceToken, ^{
        _instance = [[SSLoginManager alloc] init];
    });
    return _instance;
}

- (BOOL)checLogin {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *account = [user valueForKey:@"kSSLOGIN_ACCOUNT"];
    NSString *password = [user valueForKey:@"kSSLOGIN_PASSWORD"];
    if (!account.length || !password.length) {
        return NO;
    }
    BOOL isDisconnected = [GMCXMPPManager standardDefault].xmppStream.isDisconnected;
    if (isDisconnected) {
        [[GMCXMPPManager standardDefault] loginXMPPJID:[XMPPJID jidWithUser:account domain:userDomain resource:nil] password:password];
    }
    return YES;
}

- (void)showLogin:(loginComplete)complete {
    SSLoginViewController *login = [[SSLoginViewController alloc] init];
    SSNavController *nav = [[SSNavController alloc] initWithRootViewController:login];
    login.navigationItem.title = @"登录";
    login.loginComplete = ^{
        complete(YES);
    };
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:nil];
}

- (void)logout:(loginComplete)complete {
    [[GMCXMPPManager standardDefault] logout];
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    [user setValue:nil forKey:@"kSSLOGIN_ACCOUNT"];
    [user setValue:nil forKey:@"kSSLOGIN_PASSWORD"];
    complete ? complete(YES) : nil;
}

- (NSDictionary *)getUserInfo {
    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
    NSString *account = [user valueForKey:@"account"];
    NSString *password = [user valueForKey:@"password"];
    if (!account.length || !password.length) {
        return @{};
    }
    return @{@"account": account, @"password":password};
}

@end
