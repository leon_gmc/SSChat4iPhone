//
//  SSLoginViewController.h
//  SSChat
//
//  Created by Gmc on 2019/8/6.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSLoginViewController : SSBaseViewController
@property(copy,nonatomic)void(^loginComplete)(void);
@end

NS_ASSUME_NONNULL_END
