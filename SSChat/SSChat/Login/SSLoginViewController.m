//
//  SSLoginViewController.m
//  SSChat
//
//  Created by Gmc on 2019/8/6.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSLoginViewController.h"
#import <Masonry/Masonry.h>
#import "GMCXMPPManager.h"
#import "SSRegistViewController.h"
#import "SSToast.h"

@interface SSLoginViewController ()<UITextFieldDelegate, GMCXMPPManagerAccountProtol>
@property(nonatomic,strong)UILabel *accountLabel;
@property(nonatomic,strong)UILabel *passLabel;
@property(nonatomic,strong)UITextField *account;
@property(nonatomic,strong)UITextField *password;
@property(nonatomic,strong)UIButton *regist;
@property(nonatomic,strong)UIButton *login;

@end

@implementation SSLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initConstraints];
    [[GMCXMPPManager standardDefault] addAccountDelegate:self];
}

- (void)registAction {
    SSRegistViewController *regist = [[SSRegistViewController alloc] init];
    regist.navigationItem.title = @"注册";
    regist.registCallback = ^(NSDictionary * _Nonnull map) {
        NSString *account = [map valueForKey:@"account"];
        NSString *password = [map valueForKey:@"password"];
        self.account.text = account;
        self.password.text = password;
        [self registAccount];
    };
    [self.navigationController pushViewController:regist animated:YES];
}

- (void)registAccount {
    [[GMCXMPPManager standardDefault] registXMPPJID:[XMPPJID jidWithUser:self.account.text domain:userDomain resource:nil] password:self.password.text];
}

- (void)regist:(NSDictionary *)info {
    NSString *error = [info valueForKey:@"error"];
    if (!error.length) {
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setValue:self.account.text forKey:@"kSSLOGIN_ACCOUNT"];
        [user setValue:self.account.text forKey:@"kSSLOGIN_PASSWORD"];
        [self.navigationController popViewControllerAnimated:YES];
        [self loginAction];
    } else {
        [SSToast showToast:@"注册失败" inView:self.view];
    }
}

- (void)loginAction {
    
    if (!self.account.text.length) {
        return;
    } else if (!self.password.text.length) {
        return;
    }
    [self.view endEditing:YES];
    [[GMCXMPPManager standardDefault] loginXMPPJID:[XMPPJID jidWithUser:self.account.text domain:userDomain resource:nil] password:self.password.text];
}

- (void)login:(NSDictionary *)info {
    NSString *error = [info valueForKey:@"error"];
    if (!error.length) {
        NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
        [user setValue:self.account.text forKey:@"kSSLOGIN_ACCOUNT"];
        [user setValue:self.password.text forKey:@"kSSLOGIN_PASSWORD"];
        [self.navigationController popViewControllerAnimated:YES];
        self.loginComplete ? self.loginComplete() : nil;
    } else {
        [SSToast showToast:@"登陆失败" inView:self.view];
    }
    
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField {
//
//}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - 设置界面
- (void)initUI {
    
    self.accountLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = @"账号: ";
        label.textColor = UIColor.whiteColor;
        [self.view addSubview:label];
        label;
    });
    
    self.passLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = @"密码: ";
        label.textColor = UIColor.whiteColor;
        [self.view addSubview:label];
        label;
    });
    
    self.account = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.borderStyle = UITextBorderStyleRoundedRect;
        filed.delegate = self;
        filed.placeholder = @"请输入用户名";
        [self.view addSubview:filed];
        filed;
    });
    
    self.password = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.delegate = self;
        filed.placeholder = @"请输入密码";
        filed.borderStyle = UITextBorderStyleRoundedRect;
        filed.secureTextEntry = YES;
        [self.view addSubview:filed];
        filed;
    });
    
    self.regist = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"去注册" forState:0];
        btn.layer.cornerRadius = 20;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = UIColor.blueColor;
//        UIImage imageWithColor:UIColor size:<#(CGSize)#>
        [btn setTitleColor:UIColor.whiteColor forState:0];
        [btn addTarget:self action:@selector(registAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn;
    });
    
    
    self.login = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.layer.cornerRadius = 20;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = UIColor.blueColor;
        [btn setTitle:@"登陆" forState:0];
        [btn setTitleColor:UIColor.whiteColor forState:0];
        [btn addTarget:self action:@selector(loginAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn;
    });
    
}

- (void)initConstraints {
    
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.top.equalTo(self.view).offset(80);
        make.width.mas_equalTo(50);
    }];
    
    [self.account mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.accountLabel.mas_right);
        make.centerY.equalTo(self.accountLabel);
        make.right.equalTo(self.view).offset(-30);
        make.height.mas_equalTo(40);
    }];
    
    [self.passLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.accountLabel);
        make.top.equalTo(self.accountLabel.mas_bottom).offset(30);
    }];
    
    [self.password mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.account);
        make.height.equalTo(self.account);
        make.centerY.equalTo(self.passLabel);
    }];
    
    [self.login mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.password.mas_bottom).offset(60);
        make.size.mas_equalTo(CGSizeMake(120, 40));
    }];
    
    [self.regist mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.login.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake(120, 40));
    }];
    
    
}


@end
