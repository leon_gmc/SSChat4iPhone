//
//  SSRegistViewController.h
//  SSChat
//
//  Created by Gmc on 2019/8/7.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface SSRegistViewController : SSBaseViewController
@property(nonatomic,copy)void(^registCallback)(NSDictionary *map);
@end

NS_ASSUME_NONNULL_END
