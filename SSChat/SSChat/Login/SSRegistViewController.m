//
//  SSRegistViewController.m
//  SSChat
//
//  Created by Gmc on 2019/8/7.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSRegistViewController.h"
#import <Masonry/Masonry.h>
#import "SSToast.h"

@interface SSRegistViewController ()<UITextFieldDelegate>
@property(nonatomic,strong)UILabel *accountLabel;
@property(nonatomic,strong)UILabel *passLabel;
@property(nonatomic,strong)UILabel *repassLabel;
@property(nonatomic,strong)UITextField *account;
@property(nonatomic,strong)UITextField *password;
@property(nonatomic,strong)UITextField *repassword;
@property(nonatomic,strong)UIButton *regist;
@end

@implementation SSRegistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initConstraints];
}

- (void)registAction {
    [self.view endEditing:YES];
    if (!self.account.text.length) {
        ///提示account 不能为空
        [SSToast showToast:@"账户不能为空" inView:self.view];
        return;
    } else if (!self.password.text.length) {
        ///提示password 不能为空
        [SSToast showToast:@"密码不能为空" inView:self.view];
        return;
    } else if (!self.repassword.text.length || ![self.password.text isEqualToString:self.repassword.text]) {
        ///提示account 不能为空
        [SSToast showToast:@"两次密码不一致" inView:self.view];
        return;
    }
    
    self.registCallback ? self.registCallback(@{@"account":self.account.text, @"password":self.password.text}) : nil;
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

//- (void)textFieldDidEndEditing:(UITextField *)textField {
//
//}



#pragma mark - 设置界面
- (void)initUI {
    self.accountLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = @"账号: ";
        [self.view addSubview:label];
        label;
    });
    
    self.passLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = @"密码: ";
        [self.view addSubview:label];
        label;
    });
    
    self.repassLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = @"密码: ";
        [self.view addSubview:label];
        label;
    });
    
    self.account = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.borderStyle = UITextBorderStyleRoundedRect;
        filed.delegate = self;
        filed.placeholder = @"请输入用户名";
        [self.view addSubview:filed];
        filed;
    });
    
    self.password = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.delegate = self;
        filed.placeholder = @"请输入密码";
        filed.borderStyle = UITextBorderStyleRoundedRect;
        filed.secureTextEntry = YES;
        [self.view addSubview:filed];
        filed;
    });
    
    self.repassword = ({
        UITextField *filed = [[UITextField alloc] init];
        filed.delegate = self;
        filed.placeholder = @"请再次输入密码";
        filed.borderStyle = UITextBorderStyleRoundedRect;
        filed.secureTextEntry = YES;
        [self.view addSubview:filed];
        filed;
    });
    
    self.regist = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"注册" forState:0];
        btn.layer.cornerRadius = 20;
        btn.layer.masksToBounds = YES;
        btn.backgroundColor = UIColor.blueColor;
        [btn setTitleColor:UIColor.whiteColor forState:0];
        [btn addTarget:self action:@selector(registAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
        btn;
    });
    
}

- (void)initConstraints {
    [self.accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(30);
        make.top.equalTo(self.view).offset(80);
        make.width.mas_equalTo(50);
    }];
    
    [self.account mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.accountLabel.mas_right);
        make.centerY.equalTo(self.accountLabel);
        make.right.equalTo(self.view).offset(-30);
        make.height.mas_equalTo(40);
    }];
    
    [self.passLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.accountLabel);
        make.top.equalTo(self.accountLabel.mas_bottom).offset(30);
    }];
    
    [self.password mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.account);
        make.height.equalTo(self.account);
        make.centerY.equalTo(self.passLabel);
    }];
    
    [self.repassLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.passLabel);
        make.top.equalTo(self.passLabel.mas_bottom).offset(30);
    }];
    
    [self.repassword mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.equalTo(self.password);
        make.height.equalTo(self.password);
        make.centerY.equalTo(self.repassLabel);
    }];
    
    [self.regist mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.repassword.mas_bottom).offset(30);
        make.size.mas_equalTo(CGSizeMake(120, 40));
    }];
}

@end
