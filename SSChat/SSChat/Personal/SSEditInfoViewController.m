//
//  SSEditInfoViewController.m
//  SSChat
//
//  Created by Gmc on 2019/8/21.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSEditInfoViewController.h"
#import "GMCRoute.h"

@interface SSEditInfoViewController ()<UITextFieldDelegate>

@property(nonatomic,strong)UITextField *inputField;
@property(nonatomic,copy)NSString *type;

@end

@implementation SSEditInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initConstraints];
    NSDictionary *dict = [[GMCRoute shared] getDataForSource:self];
    self.type = [dict valueForKey:@"type"];
    if ([self.type isEqualToString:@"1"]) {
        self.inputField.placeholder = @"昵称最长为5个字符!";
    } else {
        self.inputField.placeholder = @"简介最长为15个字符!";
    }
}

- (void)textFieldDidChanged:(UITextField *)textField {
    if (textField.markedTextRange) {
        
    } else {
        if ([self.type isEqualToString:@"1"]) {
            if (textField.text.length > 5) {
                textField.text = [textField.text substringToIndex:5];
            }
        } else {
            if (textField.text.length > 15) {
                textField.text = [textField.text substringToIndex:15];
            }
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.type isEqualToString:@"1"]) {
        if (textField.text.length > 5) {
            return NO;
        }
    } else {
        if (textField.text.length > 15) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField endEditing:YES];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    NSDictionary *map = @{@"type":self.type, @"text":textField.text};
    [self sendMessageToParent:map];
}

#pragma mark - 设置界面
- (void)initUI {
    self.inputField = ({
        UITextField *textField = [[UITextField alloc] init];
        textField.delegate = self;
        textField.placeholder = @"";
        [textField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.returnKeyType = UIReturnKeyDone;
        [self.view addSubview:textField];
        textField;
    });
}

- (void)initConstraints {
    self.inputField.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *horizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-15-[_inputField]-15-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_inputField)];
    NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_inputField(==44)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_inputField)];
    [self.view addConstraints:horizontal];
    [self.view addConstraints:vertical];
}

@end
