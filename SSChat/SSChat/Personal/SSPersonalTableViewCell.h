//
//  SSPersonalTableViewCell.h
//  SSChat
//
//  Created by Gmc on 2019/8/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SSPersonalTableViewCell : SSBaseTableViewCell
@property(nonatomic,strong)NSDictionary *map;
@property(nonatomic,strong)NSIndexPath *indexPath;
@property(nonatomic,copy)void(^iconChoseBlock)(void);
@end

NS_ASSUME_NONNULL_END
