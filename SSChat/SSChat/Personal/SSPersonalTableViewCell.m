//
//  SSPersonalTableViewCell.m
//  SSChat
//
//  Created by Gmc on 2019/8/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSPersonalTableViewCell.h"
#import <Masonry/Masonry.h>

@interface SSPersonalTableViewCell ()
@property(nonatomic,strong)UIImageView *iconImageView;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *nickLabel;
@property(nonatomic,strong)UILabel *descLabel;
@end

@implementation SSPersonalTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

- (void)tapGesture {
    self.iconChoseBlock ? self.iconChoseBlock() : nil;
}

- (void)setIndexPath:(NSIndexPath *)indexPath {
    _indexPath = indexPath;
    switch (indexPath.row) {
        case 0:
        {
            self.iconImageView.hidden = NO;
            self.nickLabel.hidden = YES;
            self.nameLabel.hidden = YES;
            self.descLabel.hidden = YES;
            NSData *data = [self.map valueForKey:@"iamgeData"];
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                self.iconImageView.image = image;
            }
        }
            break;
        case 1:
        {
            self.iconImageView.hidden = YES;
            self.nickLabel.hidden = YES;
            self.nameLabel.hidden = NO;
            self.descLabel.hidden = YES;
            NSString *name = [self.map valueForKey:@"name"];
            if ([name containsString:userDomainMore]) {
                name = [name stringByReplacingOccurrencesOfString:userDomainMore withString:@""];
            }
            self.nameLabel.text = [NSString stringWithFormat:@"姓名: %@", name];
        }
            break;
        case 2:
        {
            self.iconImageView.hidden = YES;
            self.nickLabel.hidden = NO;
            self.nameLabel.hidden = YES;
            self.descLabel.hidden = YES;
            NSString *nick = [self.map valueForKey:@"nick"];
            if (nick.length) {
                self.nickLabel.text = [NSString stringWithFormat:@"昵称: %@", nick];;
            } else {
                self.nickLabel.text = @"昵称: 该用户暂未有昵称";
            }

        }
            break;
        case 3:
        {
            self.iconImageView.hidden = YES;
            self.nickLabel.hidden = YES;
            self.nameLabel.hidden = YES;
            self.descLabel.hidden = NO;
            NSString *desc = [self.map valueForKey:@"desc"];
            if (desc.length) {
                self.descLabel.text = [NSString stringWithFormat:@"简介: %@", desc];
            } else {
                self.descLabel.text = @"简介: 该用户暂未写过简介";
            }

        }
            break;
    }
}

- (void)initUI {
    
    self.iconImageView = ({
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_defaulthead_nor"]];
        [self.contentView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
        [imageView addGestureRecognizer:tap];
        imageView.layer.cornerRadius = 10;
        imageView.layer.masksToBounds = YES;
        imageView;
    });
    
    self.nameLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:label];
        label;
    });
    
    self.nickLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:16];
        label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:label];
        label;
    });
    
    self.descLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:label];
        label;
    });
}

- (void)initConstraints {
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(20);
        make.size.mas_equalTo((CGSize){100, 100});
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(20);
        make.width.mas_lessThanOrEqualTo(120);
    }];
    
    [self.nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(20);
    }];
    
    [self.descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(20);
    }];
}

@end
