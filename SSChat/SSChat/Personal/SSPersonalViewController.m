//
//  SSPersonalViewController.m
//  SSChat
//
//  Created by Gmc on 2019/8/13.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSPersonalViewController.h"
#import "GMCXMPPManager.h"
#import <Masonry/Masonry.h>
#import "GMCImagePicker.h"
#import "SSPersonalTableViewCell.h"
#import "GMCRoute.h"
#import "SSLoginManager.h"
#import "AppDelegate.h"
#import "AppDelegate+UIAppDelegate.h"


@interface SSPersonalViewController ()<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)NSDictionary *map;
@property(nonatomic,assign)BOOL couldEdit;



@end

@implementation SSPersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initRightBarButton];
    [self initUI];
    [self initConstraints];
    self.map = [[GMCXMPPManager standardDefault] getPersonalData];
    
}

- (void)initRightBarButton {
    UIButton *addBtn = ({
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:@"登出" forState:UIControlStateNormal];
        [btn setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
        btn;
    });
    [addBtn sizeToFit];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithCustomView:addBtn];
    self.navigationItem.rightBarButtonItem = barButton;
}

- (void)logout {
    [[SSLoginManager standard] logout:^(BOOL isSuccess) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate initWindow];
    }];
}

- (void)sendMessageToParent:(NSObject *)object {
    
    NSDictionary *map = (NSDictionary *)object;
    NSString *type = [map valueForKey:@"type"];
    NSString *info = [map valueForKey:@"text"];
    if (!info.length) {
        return;
    }
    [[GMCXMPPManager standardDefault] refreshPersonalInfo:info type:type];
    self.map = [[GMCXMPPManager standardDefault] getPersonalData];
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SSPersonalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[SSPersonalTableViewCell getIdentify] forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.map = self.map;
    cell.indexPath = indexPath;
    cell.iconChoseBlock = ^{
        [self showBig];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [[GMCImagePicker sharedManager] imagePickerCompletionAtType:GMCImagePickerStyleDefault needSheet:YES ViewController:self Completion:^(UIImage * _Nullable finalImage) {
            [[GMCXMPPManager standardDefault] refreshAvatar:finalImage completeBlock:^(BOOL isSuccess) {
                if (isSuccess) {
                    self.map = [[GMCXMPPManager standardDefault] getPersonalData];
                    [self.tableView reloadData];
                }
            }];
        }];
    } else if (indexPath.row == 2) {
        [[GMCRoute shared] jumpList:@"PersonalEdit" data:@{@"type":@"1"} source:self];
    } else if (indexPath.row == 3) {
        [[GMCRoute shared] jumpList:@"PersonalEdit" data:@{@"type":@"2"} source:self];
    }
}

- (void)showBig {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 120;
    }
    
    return 60;
}

#pragma mark - 设置界面
- (void)initUI {
    
    self.tableView = ({
        UITableView *table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        table.delegate = self;
        table.dataSource = self;
        [table registerClass:[SSPersonalTableViewCell class] forCellReuseIdentifier:[SSPersonalTableViewCell getIdentify]];
        table.tableFooterView = [UIView new];
        [self.view addSubview:table];
        table;
    });
    
    
}

- (void)initConstraints {
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

@end
