//
//  GMCRoute.h
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMCRoute : NSObject

+ (instancetype)shared;

- (void)loadRootList:(NSString *)rootList root:(UIViewController *)root;

- (void)jumpList:(NSString *)jumpList data:(nullable NSDictionary *)data source:(NSObject *)obj;

- (NSDictionary *)getDataForSource:(UIViewController *)controller;

@end

NS_ASSUME_NONNULL_END
