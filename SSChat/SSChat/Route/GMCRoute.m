//
//  GMCRoute.m
//  SSChat
//
//  Created by Gmc on 2019/5/18.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCRoute.h"
#import <objc/runtime.h>
@implementation GMCRoute

+ (instancetype)shared {
    static dispatch_once_t onceToken;
    static id instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[GMCRoute alloc] init];
    });
    return instance;
}

- (NSDictionary *)getDataForSource:(UIViewController *)controller {
    NSDictionary *map = objc_getAssociatedObject(controller, @"data");
    return map;
}

- (UIViewController *)findClosestResponder:(NSObject *)source {
    
    UIResponder *responder = (UIResponder *)source;
    while (responder.nextResponder) {
        if ([source isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)source;
        } else {
            responder = responder.nextResponder;
        }
    }
    return (UIViewController *)responder;
}

- (void)jumpList:(NSString *)jumpList data:(nullable NSDictionary *)data source:(NSObject *)obj{
    UIViewController *source = nil;
    if ([obj isKindOfClass:[UIViewController class]]) {
        source = (UIViewController *)obj;
    } else {
       source = [self findClosestResponder:obj];
    }
    NSString *destinationName = nil;
    NSString *jumpWay = @"push";
    NSString *path = [[NSBundle mainBundle] pathForResource:jumpList ofType:@"plist"];
    NSArray *jumpArray = [NSArray arrayWithContentsOfFile:path];
    for (NSDictionary *map in jumpArray) {
        if ([[map allKeys] containsObject:@"destination"]) {
            destinationName = [map valueForKey:@"destination"];
        }
        if ([[map allKeys] containsObject:@"way"]) {
            NSString *way = [map valueForKey:@"way"];
            if (way) {
                jumpWay = [way lowercaseString];
            }
        }
    }
    if (destinationName) {
        Class clz = NSClassFromString(destinationName);
        UIViewController *destination = [[clz alloc] init];
        if ([data allKeys]) {
            objc_setAssociatedObject(destination, @"data", data, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
        if ([jumpWay isEqualToString:@"modal"]) {
            [source presentViewController:destination animated:YES completion:nil];
        } else {
            if (source.navigationController) {
                [source.navigationController pushViewController:destination animated:YES];
            }
        }
    }
}

- (void)loadRootList:(NSString *)rootList root:(UIViewController *)root; {
    NSString *navName = @"";
    NSString *path = [[NSBundle mainBundle] pathForResource:rootList ofType:@"plist"];
    NSArray *rootArray = [NSArray arrayWithContentsOfFile:path];
    for(NSInteger i = 0; i < rootArray.count; i ++){
        NSDictionary *map = rootArray[i];
        UIViewController *controller;
        for (NSString *obj in [map allKeys]) {
            if ([obj isEqualToString:@"name"]) {
                NSString *className = [map valueForKey:obj];
                Class clz = NSClassFromString(className);
                if ([[clz new] isKindOfClass:[UINavigationController class]]) {
                    navName = className;
                } else {
                    controller = [[clz alloc] init];
                    Class clz = NSClassFromString(navName);
                    UINavigationController *nav = [[clz alloc] initWithRootViewController:controller];
                    [root addChildViewController:nav];
                }
                ;
            } else if ([obj isEqualToString:@"title"]) {
                NSString *title = [map valueForKey:obj];
                controller.title = title;
            } else if ([obj isEqualToString:@"icon"]) {
                NSString *icon = [map valueForKey:obj];
                UIImage *image = [UIImage imageNamed:icon];
                controller.tabBarItem.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                UIImage *selectedImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected", icon]];
                controller.tabBarItem.selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//                controller.tabBarItem.imageInsets = UIEdgeInsetsMake(4, 0, 0, 0);
            }
        }
    }
}

@end
