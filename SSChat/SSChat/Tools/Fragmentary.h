//
//  Fragmentary.h
//  SSPhotoScanDemo
//
//  Created by Gmc on 2019/2/26.
//  Copyright © 2019 GMC. All rights reserved.
//


// 高度812
#define W [[UIScreen mainScreen] bounds].size.width
#define H [[UIScreen mainScreen] bounds].size.height
#define isXs H > 812 ? YES : NO
#define StatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define NavigationBarHeight (StatusBarHeight + 44)
#define TabbarHeight isXs ? 82 : 49
#define useHostPort 5222
#define useHostName @"192.168.3.2"
#define userDomain @"embrace-changed"
#define userDomainMore [NSString stringWithFormat:@"@%@", userDomain]

