//
//  GMCFaceManager.h
//  SSChat
//
//  Created by Gmc on 2019/6/5.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMCFaceManager : NSObject
+ (instancetype)shared;

- (NSArray *)getEmojis;

- (NSArray *)getFaceArray;

- (void)load;
@end

NS_ASSUME_NONNULL_END
