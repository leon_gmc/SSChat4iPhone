//
//  GMCFaceManager.m
//  SSChat
//
//  Created by Gmc on 2019/6/5.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCFaceManager.h"
#import "GMCFaceModel.h"


@interface GMCFaceManager ()

@property(nonatomic,strong)NSMutableArray *faces;
@property(nonatomic,strong)NSMutableArray *emojis;
@property(nonatomic,strong)NSMutableArray *entity;


@end

@implementation GMCFaceManager

static GMCFaceManager *instance = nil;
+ (instancetype)shared {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)load {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"faces_icons.plist" ofType:nil];
    NSArray* array = [NSArray arrayWithContentsOfFile:path];
    [self.entity removeAllObjects];
    for(int i = 0; i < array.count; i ++){
        NSDictionary *map = array[i];
        GMCFaceModel *entity = [GMCFaceModel entityWithDictionary:map atIndex:i];
        [self.entity addObject:entity];
    }
    for (CGFloat i = 0; i < self.entity.count; i ++) {
        if (fmodf(i + 1, 24) == 0) {
            GMCFaceModel *entity = [[GMCFaceModel alloc] init];
            entity.name = @"delete_btn";
            entity.code = @"delete_btn";
            entity.imageName = @"delete_btn";
            [self.entity insertObject:entity atIndex:i];
        }
    }
    if (fmod(self.entity.count, 24.0) != 0) {
        int value = (24 - fmod(self.entity.count, 24.0));
        for (int i = 0; i < value; i ++) {
            GMCFaceModel *entity = [[GMCFaceModel alloc] init];
            entity.name = @"";
            entity.code = @"";
            entity.imageName = @"";
            [self.entity addObject:entity];
        }
    }
    [self.entity removeLastObject];
    GMCFaceModel *entity = [[GMCFaceModel alloc] init];
    entity.name = @"delete_btn";
    entity.code = @"delete_btn";
    entity.imageName = @"delete_btn";
    [self.entity addObject:entity];
    
    
}

//- (NSArray<UIImage*> *)getEmojis {
//
//
//}

- (NSArray *)getFaceArray {
    
    return [self.entity copy];
}


- (NSMutableArray *)emojis {
    if (!_emojis) {
        _emojis = [[NSMutableArray alloc] init];
    }
    return _emojis;
}

- (NSMutableArray *)faces {
    if (!_faces) {
        _faces = [[NSMutableArray alloc] init];
    }
    return _faces;
}

- (NSMutableArray *)entity {
    if (!_entity) {
        _entity = [[NSMutableArray alloc] init];
    }
    return _entity;
}


@end

