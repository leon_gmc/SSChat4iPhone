//
//  GMCImagePicker.h
//  SSChat
//
//  Created by Gmc on 2019/6/13.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class UIImage, UIViewController;
typedef void(^GMCImagePickerCompletion)(UIImage * _Nullable finalImage);
typedef NS_ENUM(NSInteger, GMCImagePickerStyle) {
    GMCImagePickerStyleDefault,          // default is album and camera
    GMCImagePickerStyleSingleAlbum,      // album only
    GMCImagePickerStyleSingleCamera,     // camera only
};


@interface GMCImagePicker : NSObject

+ (instancetype)sharedManager;

/**
 图片获取
 
 @param type 当need为YES时 会获取对应的m功能面板 当need为NO时, 只会触发对应的功能
 @param need 是否需要提示面板
 @param viewController 当前弹出的vc, 若没有可用rootVc
 @param completion 成功回调
 */
- (void)imagePickerCompletionAtType:(GMCImagePickerStyle)type needSheet:(BOOL)need ViewController:(UIViewController *)viewController Completion:(GMCImagePickerCompletion)completion;

@property(nonatomic,assign)BOOL needCrop;


@end

NS_ASSUME_NONNULL_END
