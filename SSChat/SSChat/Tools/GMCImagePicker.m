//
//  GMCImagePicker.m
//  SSChat
//
//  Created by Gmc on 2019/6/13.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCImagePicker.h"

@interface GMCImagePicker () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, copy) GMCImagePickerCompletion completion;

@end

@implementation GMCImagePicker

+ (instancetype)sharedManager {
    static GMCImagePicker *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[GMCImagePicker alloc] init];
    });
    
    return _sharedManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)imagePickerCompletionAtType:(GMCImagePickerStyle)type ViewController:(UIViewController *)viewController Completion:(GMCImagePickerCompletion)completion {
    self.completion = completion;
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.allowsEditing = NO;
    imagePickerController.delegate = self;
    if (type == GMCImagePickerStyleSingleCamera) {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            
        } else {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [viewController presentViewController:imagePickerController animated:YES completion:nil];
    } else if (type == GMCImagePickerStyleSingleAlbum) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [viewController presentViewController:imagePickerController animated:YES completion:nil];
    }
}
- (void)imagePickerCompletionAtType:(GMCImagePickerStyle)type needSheet:(BOOL)need ViewController:(UIViewController *)viewController Completion:(GMCImagePickerCompletion)completion {
    if (need) {
        [self imagePickerNeedSheetCompletionAtType:type ViewController:viewController Completion:completion];
    } else {
        [self imagePickerCompletionAtType:type ViewController:viewController Completion:completion];
    }
}


- (void)imagePickerNeedSheetCompletionAtType:(GMCImagePickerStyle)type ViewController:(UIViewController *)viewController Completion:(GMCImagePickerCompletion)completion {
    self.completion = completion;
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.allowsEditing = NO;
    imagePickerController.delegate = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *callCameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear] || [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            
        } else {
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [viewController presentViewController:imagePickerController animated:YES completion:nil];
        
    }];
    
    UIAlertAction *callAlbumAction = [UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [viewController presentViewController:imagePickerController animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    
    
    
    [alertController setModalPresentationStyle:UIModalPresentationPopover];
    if (@available(iOS 9.0, *)) {
        [callAlbumAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [callCameraAction setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [cancelAction setValue:[UIColor grayColor] forKey:@"titleTextColor"];
    }
    
    switch (type) {
        case GMCImagePickerStyleDefault:
        {
            [alertController addAction:callCameraAction];
            [alertController addAction:callAlbumAction];
            [alertController addAction:cancelAction];
        }
            break;
        case GMCImagePickerStyleSingleAlbum:
        {
            [alertController addAction:callAlbumAction];
            [alertController addAction:cancelAction];
        }
            break;
        case GMCImagePickerStyleSingleCamera:
        {
            [alertController addAction:callCameraAction];
            [alertController addAction:cancelAction];
        }
            break;
        default:
            break;
    }
    
    [viewController presentViewController:alertController animated:YES completion:nil];
    
}



#pragma mark Private Methods

#pragma mark UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (self.needCrop) {
        
    } else {
        self.completion(image);
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
//    SSCropViewController *cropController = [[SSCropViewController alloc]initWithCustomerToolbar:NO image:image style:SSCropViewCroppingStyleDefault];
//    cropController.delegate = self;
//    cropController.toolbar.rotateCounterclockwiseButtonHidden = YES;
//    cropController.toolbar.resetButton.hidden = YES;
//    cropController.toolbar.clampButtonHidden = YES;
//    cropController.toolbar.rotateClockwiseButtonHidden = YES;
//    cropController.toolbar.doneTextButtonTitle = @"完成";
//    cropController.toolbar.cancelTextButtonTitle = @"取消";
//    //    [cropController setAspectRatioPreset:TOCropViewControllerAspectRatioPresetSquare];
//    [cropController setAspectRatioPreset:SSCropViewControllerAspectRatioPresetSquare];
//    [picker pushViewController:cropController animated:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    self.completion(nil);
    [picker dismissViewControllerAnimated:YES completion:nil];
}

//- (void)cropViewController:(SSCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle {
//    self.completion(image);
//    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
//}
//
//- (void)cropViewController:(SSCropViewController *)cropViewController didFinishCancelled:(BOOL)cancelled {
//    if (cancelled) {
//        UIImagePickerController *picker = (UIImagePickerController *)cropViewController.navigationController;
//        self.completion(nil);
//        [picker dismissViewControllerAnimated:YES completion:nil];
//    }
//}


@end
