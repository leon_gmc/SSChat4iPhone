//
//  GMCMPMManager.h
//  SSChat
//
//  Created by Gmc on 2019/10/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GMCMPMManager : NSObject
+ (instancetype)standardDefault;

- (void)joinRoom:(NSString *)roomName nickName:(NSString *)nickName;
@end

NS_ASSUME_NONNULL_END
