//
//  GMCMPMManager.m
//  SSChat
//
//  Created by Gmc on 2019/10/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCMPMManager.h"
#import "GMCXMPPManager.h"

@interface GMCMPMManager ()<XMPPRoomDelegate>
@property(nonatomic,strong)XMPPMUC *muc;
@property(nonatomic,strong)NSMutableDictionary *roomMap;

@end

@implementation GMCMPMManager

+ (instancetype)standardDefault {
    static GMCMPMManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    return _instance;
}

- (void)joinRoom:(NSString *)roomName nickName:(NSString *)nickName; {
//    XMPPRoom *room = [[XMPPRoom alloc] initWithRoomStorage:[XMPPRoomCoreDataStorage sharedInstance] jid:[XMPPJID jidWithUser:roomName domain:@"private.embrace-changed" resource:nil]];
//    [self.roomMap setValue:room forKey:roomName];
//    [room addDelegate:self delegateQueue:dispatch_get_main_queue()];
//    XMPPStream *stream = [GMCXMPPManager standardDefault].xmppStream;
//    [room activate:stream];
//    [room joinRoomUsingNickname:nickName history:nil];
}

///加入成功
- (void)xmppRoomDidJoin:(XMPPRoom *)sender {
    NSLog(@"");
}

///创建成功
- (void)xmppRoomDidCreate:(XMPPRoom *)sender {
////    [sender fetchConfigurationForm];///模板样式
//    [sender configureRoomUsingOptions:nil];
//    ///invite
//    [sender inviteUser:[XMPPJID jidWithUser:@"erwa" domain:@"embrace-changed" resource:nil] withMessage:@"大家一起来聊天"];
}

//离开
- (void)xmppRoomDidLeave:(XMPPRoom *)sender {
    
}

//销毁
- (void)xmppRoomDidDestroy:(XMPPRoom *)sender {
    
}


- (XMPPMUC *)muc {
    if (!_muc) {
        _muc = [[XMPPMUC alloc] initWithDispatchQueue:dispatch_get_main_queue()];
    }
    return _muc;
}

- (NSMutableDictionary *)roomMap {
    if (!_roomMap) {
        _roomMap = [[NSMutableDictionary alloc] init];
    }
    return _roomMap;
}

@end
