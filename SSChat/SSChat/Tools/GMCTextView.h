//
//  GMCTextView.h
//  SSChat
//
//  Created by Gmc on 2019/5/26.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^textHeightChangedBlock)(CGFloat textHeight);
@interface GMCTextView : UITextView
@property (nonatomic, assign) CGFloat maxTextH;
@property (nonatomic, assign) NSUInteger maxNumberOfLines;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, strong) textHeightChangedBlock textChangedBlock;
- (void)textValueDidChanged:(textHeightChangedBlock)block;

@end

NS_ASSUME_NONNULL_END
