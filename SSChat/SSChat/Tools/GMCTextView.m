//
//  GMCTextView.m
//  SSChat
//
//  Created by Gmc on 2019/5/26.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCTextView.h"
static float textFont = 25;

@interface GMCTextView ()


@end

@implementation GMCTextView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        if (!self.font) {
            self.font = [UIFont systemFontOfSize:textFont];
        }
        [self initUI];
        [self initConstraints];
    }
    return self;
}

#pragma mark - 设置界面
- (void)initUI {
    
    self.scrollEnabled = YES;
    self.scrollsToTop = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.enablesReturnKeyAutomatically = YES;
    self.returnKeyType = UIReturnKeySend;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:self];
}

- (void)textDidChange {
    //计算高度
    CGFloat height = ceilf([self sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)].height);
    if (height > _maxTextH) {
        height = _maxTextH;
        self.scrollEnabled = YES;   //当textView大于最大高度的时候让其可以滚动
    } else {
        self.scrollEnabled = NO;
        if (_textChangedBlock && self.scrollEnabled == NO) {
            _textChangedBlock(height);
        }
    }
    [self layoutIfNeeded];
}

- (void)initConstraints {
    
}

- (void)setMaxNumberOfLines:(NSUInteger)maxNumberOfLines {
    _maxNumberOfLines = maxNumberOfLines;
    _maxTextH = ceil(self.font.lineHeight * maxNumberOfLines + self.textContainerInset.top + self.textContainerInset.bottom);
}

- (void)textValueDidChanged:(textHeightChangedBlock)block {
    _textChangedBlock = block;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
