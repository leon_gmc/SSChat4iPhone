//
//  GMCXMPPManager.h
//  SSChat
//
//  Created by Gmc on 2019/7/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^completeBlock)(BOOL isSuccess);
@protocol GMCXMPPManagerPersonalInfoProtocol <NSObject>
@optional
- (void)cardRefreshed;

@end

@protocol GMCXMPPManagerAccountProtol <NSObject>
@optional
- (void)login:(NSDictionary *)info;
- (void)regist:(NSDictionary *)info;


@end

@interface GMCXMPPManager : NSObject
+ (instancetype)standardDefault;
- (void)loginXMPPJID:(XMPPJID *)jid password:(NSString *)password;
- (void)registXMPPJID:(XMPPJID *)jid password:(NSString *)password;
- (void)logout;
- (void)sendMessage:(NSObject *)message type:(NSString *)type jid:(NSString *)jid;
- (void)addNewFriend:(NSString *)friendId;
- (void)removeFriend:(XMPPJID *)friendId;
- (void)refreshAvatar:(UIImage *)image completeBlock:(completeBlock)block;
- (NSDictionary *)getPersonalData;
- (NSData *)getAvatar:(XMPPJID *)jid;
- (void)refreshPersonalInfo:(NSString *)info type:(NSString *)type;
- (void)addPersonalInfoDelegate:(id<GMCXMPPManagerPersonalInfoProtocol>)delegate;
- (void)addAccountDelegate:(id<GMCXMPPManagerAccountProtol>)delegate;

@property(nonatomic,strong)XMPPStream *xmppStream;
//@property(weak,nonatomic)id<GMCXMPPManagerProtocol> delegate;

@end

NS_ASSUME_NONNULL_END
