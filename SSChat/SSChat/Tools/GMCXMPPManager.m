//
//  GMCXMPPManager.m
//  SSChat
//
//  Created by Gmc on 2019/7/8.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "GMCXMPPManager.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "XMPPLogging.h"
#import "XMPPAutoPing.h"

@interface GMCXMPPManager ()<XMPPStreamDelegate, XMPPReconnectDelegate, XMPPAutoPingDelegate, XMPPRosterDelegate, XMPPIncomingFileTransferDelegate, XMPPOutgoingFileTransferDelegate, XMPPvCardTempModuleDelegate, XMPPvCardAvatarDelegate>
@property(nonatomic,strong)NSString *pwd;
@property(nonatomic,strong)XMPPReconnect *reconnect;
@property(nonatomic,strong)XMPPAutoPing *autoPing;
@property(nonatomic,strong)XMPPRoster *roster;
@property(nonatomic,strong)XMPPMessageArchiving *messageArchiving;
@property(nonatomic,strong)XMPPIncomingFileTransfer *incomingFileTransfer;
@property(nonatomic,strong)XMPPOutgoingFileTransfer *outgoingFileTransfer;
@property(nonatomic,strong)XMPPvCardTempModule *personalCard;
@property(nonatomic,strong)XMPPvCardAvatarModule *otherCard;
@property(nonatomic,strong)XMPPvCardTemp *card;
@property(nonatomic,  copy)completeBlock globalBlock;
@property(nonatomic,strong)NSMutableArray *personalInfoDelegateQueue;
@property(nonatomic,strong)NSMutableArray *accountDelegateQueue;

@end

@implementation GMCXMPPManager
{
    BOOL _isLogin;
}

+ (instancetype)standardDefault {
    static GMCXMPPManager *_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
        ///开启log
//        [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
    });
    return _instance;
}

- (void)addPersonalInfoDelegate:(id<GMCXMPPManagerPersonalInfoProtocol>)delegate {
    if (![self.personalInfoDelegateQueue containsObject:delegate]) {
        [self.personalInfoDelegateQueue addObject:delegate];
    }
}

- (void)addAccountDelegate:(id<GMCXMPPManagerAccountProtol>)delegate {
    if (![self.accountDelegateQueue containsObject:delegate]) {
        [self.accountDelegateQueue addObject:delegate];
    }
}
///获取个人资料
- (NSDictionary *)getPersonalData {
    NSData *iamgeData = self.card.photo;
    NSString *name = self.xmppStream.myJID.bare;
    NSString *nick = self.card.nickname;
    NSString *desc = self.card.desc;
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    [map setValue:iamgeData forKey:@"iamgeData"];
    [map setValue:name forKey:@"name"];
    [map setValue:nick forKey:@"nick"];
    [map setValue:desc forKey:@"desc"];
    return map.copy;
}

///更新个人信息
- (void)refreshPersonalInfo:(NSString *)info type:(NSString *)type {
    if ([type isEqualToString:@"1"]) {
        self.card.nickname = info;
    } else if ([type isEqualToString:@"2"]) {
        self.card.desc = info;
    }
    [self.personalCard updateMyvCardTemp:self.card];
}

///更新头像信息
- (void)refreshAvatar:(UIImage *)image completeBlock:(completeBlock)block {
    NSData *data = UIImageJPEGRepresentation(image, 0.3);
    self.card.photo = data;
    [self.personalCard updateMyvCardTemp:self.card];
    self.globalBlock = block;
}

///别人更新头像更新
- (void)xmppvCardAvatarModule:(XMPPvCardAvatarModule *)vCardTempModule didReceivePhoto:(UIImage *)photo forJID:(XMPPJID *)jid {
    for (id <GMCXMPPManagerPersonalInfoProtocol>delegate in self.personalInfoDelegateQueue) {
        if ([delegate respondsToSelector:@selector(cardRefreshed)]) {
            [delegate cardRefreshed];
        }
    }
}

///更新个人信息成功
- (void)xmppvCardTempModuleDidUpdateMyvCard:(XMPPvCardTempModule *)vCardTempModule {
    self.globalBlock ? self.globalBlock(YES) : nil;
}

///更新个人信息失败
- (void)xmppvCardTempModule:(XMPPvCardTempModule *)vCardTempModule failedToUpdateMyvCard:(DDXMLElement *)error {
    self.globalBlock ? self.globalBlock(NO) : nil;
}


///获取头像
- (NSData *)getAvatar:(XMPPJID *)jid {
    return [self.otherCard photoDataForJID:jid];
}

///添加好友
- (void)addNewFriend:(NSString *)friendId {
    [self.roster addUser:[XMPPJID jidWithUser:friendId domain:userDomain resource:nil] withNickname:nil];
}

///删除好友
- (void)removeFriend:(XMPPJID *)friendId {
    [self.roster removeUser:friendId];
}

///好友请求
- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence {
    
    NSString *from = presence.fromStr;
    NSString *fromName = [from stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"@%@",userDomain] withString:@""];
    [self.roster acceptPresenceSubscriptionRequestFrom:[XMPPJID jidWithUser:fromName domain:userDomain resource:nil] andAddToRoster:YES];
    
}

#pragma mark - 好友请求
- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterPush:(XMPPIQ *)iq {
    NSLog(@"");
}

- (void)xmppRosterDidBeginPopulating:(XMPPRoster *)sender {
    NSLog(@"");
}

- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender {
    NSLog(@"");
}

- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(NSXMLElement *)item {
    NSLog(@"");
}

///发消息
- (void)sendMessage:(NSObject *)message type:(NSString *)type jid:(NSString *)jid {
    if ([message isKindOfClass:[NSString class]]) {
        XMPPMessage *msg;
        if ([type isEqualToString:@"chat"]) {
            msg = [XMPPMessage messageWithType:type to:[XMPPJID jidWithUser:jid domain:userDomain resource:nil]];
        } else if ([type isEqualToString:@"groupchat"]) {
//            msg = [XMPPMessage messageWithType:type to:[XMPPJID jidWithUser:jid domain:@"private.embrace-changed" resource:nil]];
        }
        NSString *msgStr = (NSString *)message;
        [msg addBody:msgStr];
        [self.xmppStream sendElement:msg];
    } else if ([message isKindOfClass:[UIImage class]]) {
        UIImage *image = (UIImage *)message;
        NSData *data = UIImageJPEGRepresentation(image, 0.3);
//        [self.outgoingFileTransfer sendData:data toRecipient:[XMPPJID jidWithUser:jid domain:@"cn.gmc" resource:nil]];
        NSError *error;
        XMPPUserCoreDataStorageObject *user = [[XMPPRosterCoreDataStorage sharedInstance] userForJID:[XMPPJID jidWithUser:jid domain:userDomain resource:nil] xmppStream:self.xmppStream managedObjectContext:[XMPPRosterCoreDataStorage sharedInstance].mainThreadManagedObjectContext];
        XMPPResourceCoreDataStorageObject *resource = user.primaryResource;
        XMPPJID *fullJid = resource.jid;
        NSString *fileName = [[self.xmppStream generateUUID] stringByAppendingString:@"jpg"];
        [self.outgoingFileTransfer sendData:data named:fileName toRecipient:fullJid description:nil error:&error];
        NSLog(@"");
    }
    
}

///发送信息
- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    
}
///接收消息
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message {
    ///message.body  消息体
    
}

///登陆
- (void)loginXMPPJID:(XMPPJID *)jid password:(NSString *)password {
    _isLogin = YES;
    if (_xmppStream) {
        _xmppStream = nil;
    }
    self.pwd = password;
    [self.xmppStream setMyJID:jid];
    [self.xmppStream connectWithTimeout:-1 error:nil];
    [self active];
}

///登出
- (void)logout {
    [self.xmppStream disconnect];
    _xmppStream = nil;
}
///注册
- (void)registXMPPJID:(XMPPJID *)jid password:(NSString *)password {
    _isLogin = NO;
    if (_xmppStream) {
        _xmppStream = nil;
    }
    [self.xmppStream setMyJID:jid];
    [self.xmppStream connectWithTimeout:-1 error:nil];
    self.pwd = password;
}

///激活
- (void)active {
    [self.reconnect activate:self.xmppStream];
    [self.autoPing activate:self.xmppStream];
    [self.roster activate:self.xmppStream];
    [self.messageArchiving activate:self.xmppStream];
    [self.incomingFileTransfer activate:self.xmppStream];
    [self.outgoingFileTransfer activate:self.xmppStream];
    [self.personalCard activate:self.xmppStream];
    [self.otherCard activate:self.xmppStream];
}

///注册成功
- (void)xmppStreamDidRegister:(XMPPStream *)sender {
    if (self.accountDelegateQueue) {
        for (id<GMCXMPPManagerAccountProtol>delegate in self.accountDelegateQueue) {
            if ([delegate respondsToSelector:@selector(regist:)]) {
                [delegate regist:@{@"error":@"", @"success":@"YES", @"message":@"SUCCESS"}];
            }
        }
    }
}
///注册失败
- (void)xmppStream:(XMPPStream *)sender didNotRegister:(DDXMLElement *)error {
    if (self.accountDelegateQueue) {
        for (id<GMCXMPPManagerAccountProtol>delegate in self.accountDelegateQueue) {
            if ([delegate respondsToSelector:@selector(regist:)]) {
                [delegate regist:@{@"error":@"REGIST_FAILED", @"success":@"NO", @"message":@"注册失败"}];
            }
        }
    }
}
///连接失败
- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error {
    ///connect fail
    NSLog(@"%@", error);
}
///连接成功
- (void)xmppStreamDidConnect:(XMPPStream *)sender {
    ///connect success
    if (_isLogin) {
        [self.xmppStream authenticateWithPassword:self.pwd error:nil];
    } else {
        [self.xmppStream registerWithPassword:self.pwd error:nil];
    }
}
///授权成功
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    ///login success
    XMPPPresence *presence = [XMPPPresence presence];
    [presence addChild:[DDXMLElement elementWithName:@"type" stringValue:@"avaiable"]];
    [self.xmppStream sendElement:presence];
    if (self.accountDelegateQueue) {
        for (id<GMCXMPPManagerAccountProtol>delegate in self.accountDelegateQueue) {
            if ([delegate respondsToSelector:@selector(login:)]) {
                [delegate login:@{@"error":@"", @"success":@"YES", @"message":@"登录成功"}];
            }
        }
    }
    
}
///授权失败
- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error {
    if (self.accountDelegateQueue) {
        for (id<GMCXMPPManagerAccountProtol>delegate in self.accountDelegateQueue) {
            if ([delegate respondsToSelector:@selector(login:)]) {
                [delegate login:@{@"error":@"LOGIN_FAILED", @"success":@"NO", @"message":@"登录失败"}];
            }
        }
    }
}
///心跳超时
- (void)xmppAutoPingDidTimeout:(XMPPAutoPing *)sender {
    //MARK:提示用户 超时
}
///文件接收出错
- (void)xmppIncomingFileTransfer:(XMPPIncomingFileTransfer *)sender didFailWithError:(NSError *)error {
    
}
///确认接收文件
XMPPJID *_fromJid;
- (void)xmppIncomingFileTransfer:(XMPPIncomingFileTransfer *)sender didReceiveSIOffer:(XMPPIQ *)offer {
    //MARK:追加提示框
    _fromJid = offer.from;
    [sender acceptSIOffer:offer];
}
///文件接收成功
-(void)xmppIncomingFileTransfer:(XMPPIncomingFileTransfer *)sender didSucceedWithData:(NSData *)data named:(NSString *)name {
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:name];
    NSData *saveData = data;
    [saveData writeToFile:path atomically:YES];
    XMPPMessage *message = [XMPPMessage messageWithType:@"chat" to:self.xmppStream.myJID];
    [message addAttributeWithName:@"from" stringValue:_fromJid.bare];
    [message addBody:name];
    [message addSubject:@"FILE"];
    ///保存
    [[XMPPMessageArchivingCoreDataStorage sharedInstance] archiveMessage:message outgoing:NO xmppStream:self.xmppStream];
}

- (void)xmppOutgoingFileTransferIBBClosed:(XMPPOutgoingFileTransfer *)sender {
    
}
///发送文件成功
- (void)xmppOutgoingFileTransferDidSucceed:(XMPPOutgoingFileTransfer *)sender {
    
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:sender.outgoingFileName];
    [sender.outgoingData writeToFile:path atomically:YES];
    XMPPMessage *message = [XMPPMessage messageWithType:@"chat" to:sender.recipientJID.bareJID];
    [message addAttributeWithName:@"from" stringValue:self.xmppStream.myJID.bare];
    [message addBody:sender.outgoingFileName];
    [message addSubject:@"FILE"];
    [[XMPPMessageArchivingCoreDataStorage sharedInstance] archiveMessage:message outgoing:YES xmppStream:self.xmppStream];
}
///发送文件失败
- (void)xmppOutgoingFileTransfer:(XMPPOutgoingFileTransfer *)sender didFailWithError:(NSError *)error {
    NSLog(@"发送文件失败");
}

- (XMPPStream *)xmppStream {
    if (!_xmppStream) {
        _xmppStream = [[XMPPStream alloc] init];
        ///端口
        _xmppStream.hostPort = useHostPort;
        ///地址
        _xmppStream.hostName = useHostName;
        [_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _xmppStream;
}

- (XMPPReconnect *)reconnect {
    if (!_reconnect) {
        _reconnect = [[XMPPReconnect alloc] initWithDispatchQueue:dispatch_get_main_queue()];
        _reconnect.autoReconnect = YES;
        [_reconnect addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _reconnect;
}

- (XMPPAutoPing *)autoPing {
    if (!_autoPing) {
        _autoPing = [[XMPPAutoPing alloc] init];
        _autoPing.pingInterval = 2.0;
        _autoPing.pingTimeout = 600.0;
        [_autoPing addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _autoPing;
}

- (XMPPRoster *)roster {
    if (!_roster) {
        _roster = [[XMPPRoster alloc] initWithRosterStorage:[XMPPRosterCoreDataStorage sharedInstance] dispatchQueue:dispatch_get_main_queue()];
        //自动寻找好友
//        _roster.autoFetchRoster = YES;
        //离线清数据
        _roster.autoClearAllUsersAndResources = NO;
        //自动出席定略
//        _roster.autoAcceptKnownPresenceSubscriptionRequests = YES;
        [_roster addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _roster;
}

- (XMPPMessageArchiving *)messageArchiving {
    if (!_messageArchiving) {
        _messageArchiving = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:[XMPPMessageArchivingCoreDataStorage sharedInstance] dispatchQueue:dispatch_get_main_queue()];
    }
    return _messageArchiving;
}

- (XMPPIncomingFileTransfer *)incomingFileTransfer {
    if (!_incomingFileTransfer) {
        _incomingFileTransfer = [[XMPPIncomingFileTransfer alloc] initWithDispatchQueue:dispatch_get_main_queue()];
        _incomingFileTransfer.autoAcceptFileTransfers = YES;
        [_incomingFileTransfer addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _incomingFileTransfer;
}

- (XMPPOutgoingFileTransfer *)outgoingFileTransfer {
    if (!_outgoingFileTransfer) {
        _outgoingFileTransfer = [[XMPPOutgoingFileTransfer alloc] initWithDispatchQueue:dispatch_get_main_queue()];
        [_outgoingFileTransfer addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _outgoingFileTransfer;
}

- (XMPPvCardTempModule *)personalCard {
    if (!_personalCard) {
        _personalCard = [[XMPPvCardTempModule alloc] initWithvCardStorage:[XMPPvCardCoreDataStorage sharedInstance] dispatchQueue:dispatch_get_main_queue()];
        [_personalCard addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _personalCard;
}

- (XMPPvCardAvatarModule *)otherCard {
    if (!_otherCard) {
        _otherCard = [[XMPPvCardAvatarModule alloc] initWithvCardTempModule:self.personalCard dispatchQueue:dispatch_get_main_queue()];
        [_otherCard addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
    return _otherCard;
}

- (XMPPvCardTemp *)card {
    if (!_card) {
        _card = self.personalCard.myvCardTemp;
    }
    return _card;
}

- (NSMutableArray *)personalInfoDelegateQueue {
    if (!_personalInfoDelegateQueue) {
        _personalInfoDelegateQueue = [[NSMutableArray alloc] init];
    }
    return _personalInfoDelegateQueue;
}

- (NSMutableArray *)accountDelegateQueue {
    if (!_accountDelegateQueue) {
        _accountDelegateQueue = [[NSMutableArray alloc] init];
    }
    return _accountDelegateQueue;
}

@end
