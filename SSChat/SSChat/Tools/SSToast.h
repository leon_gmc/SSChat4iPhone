//
//  SSToast.h
//  SSChat
//
//  Created by Gmc on 2019/9/4.
//  Copyright © 2019 GMC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSToast : UIView
///指定view 显示
+ (void)showToast:(NSString *)message inView:(UIView *)view;
///window 显示
+ (void)showToast:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
