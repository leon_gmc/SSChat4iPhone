//
//  SSToast.m
//  SSChat
//
//  Created by Gmc on 2019/9/4.
//  Copyright © 2019 GMC. All rights reserved.
//

#import "SSToast.h"

static CGFloat paddingW = 10;
static CGFloat paddingH = 6;

@interface SSToast ()
@property(nonatomic,strong)UILabel *toastLabel;

@end

@implementation SSToast
{
   __weak UIView *_parent;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initUI];
        [self initConstraints];
    }
    return self;
}

+ (void)showToast:(NSString *)message inView:(UIView *)view {
    [[[self alloc] init] showToast:message inView:view];
}

- (void)showToast:(NSString *)message inView:(UIView *)view {
    self.toastLabel.text = message;
    _parent = view;
    [_parent addSubview:self];
    CGSize size = [message boundingRectWithSize:CGSizeMake(MAXFLOAT, 0) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil].size;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [_parent addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_parent attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [_parent addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_parent attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1 constant:size.width + paddingW * 2]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:0 multiplier:1 constant:size.height + paddingH * 2]];
    self.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hide];
    });
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
       self->_parent = nil;
    }];
}

+ (void)showToast:(NSString *)message {
    
}

- (void)initUI {
    self.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.5];
    self.layer.cornerRadius = 4;
    self.clipsToBounds = YES;
    self.toastLabel = ({
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:14];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        label;
    });
}

- (void)initConstraints {
    self.toastLabel.translatesAutoresizingMaskIntoConstraints = NO;
    NSArray *toastH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_toastLabel]-|" options:NSLayoutFormatAlignAllCenterX metrics:nil views:NSDictionaryOfVariableBindings(_toastLabel)];
    NSArray *toastV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_toastLabel]-|" options:NSLayoutFormatAlignAllCenterY metrics:nil views:NSDictionaryOfVariableBindings(_toastLabel)];
    [self addConstraints:toastH];
    [self addConstraints:toastV];
    
}


@end
