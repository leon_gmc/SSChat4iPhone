//
//  UIColor+SSColor.h
//  BigProject
//
//  Created by Gmc on 2018/7/21.
//  Copyright © 2018年 Gmc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (SSColor)
+ (UIColor *)colorWithRed:(CGFloat)red
                    Green:(CGFloat)green
                     Blue:(CGFloat)blue
                    alpha:(CGFloat)alpha;

+ (UIColor *)colorFromRGB:(NSInteger)rgbValue Alpha:(CGFloat)alpah;

+ (UIColor *)colorFromRGB:(NSInteger)rgbValue;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

+ (UIColor *)colorWithHexString:(NSString *)hexString Alpha:(CGFloat)alpah;
@end
