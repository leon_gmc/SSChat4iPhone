//
//  UIImage+SSImage.h
//  BigProject
//
//  Created by Gmc on 2018/7/21.
//  Copyright © 2018年 Gmc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SSImage)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
@end
