//
//  UIImage+SSImage.m
//  BigProject
//
//  Created by Gmc on 2018/7/21.
//  Copyright © 2018年 Gmc. All rights reserved.
//

#import "UIImage+SSImage.h"

@implementation UIImage (SSImage)
+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
